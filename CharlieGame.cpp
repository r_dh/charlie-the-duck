//-----------------------------------------------------------------
// Game File
// C++ Source - CharlieGame.cpp - version v2_13 jan 2014 
// Copyright Kevin Hoefman - kevin.hoefman@howest.be
// http://www.digitalartsandentertainment.be/
//-----------------------------------------------------------------

//-----------------------------------------------------------------
// Name, first name: D'Heygere, R�my
// Group: 1DAE7
//-----------------------------------------------------------------

//-----------------------------------------------------------------
// Include Files
//-----------------------------------------------------------------
#include "CharlieGame.h"																				

//-----------------------------------------------------------------
// Defines
//-----------------------------------------------------------------
#define GAME_ENGINE (GameEngine::GetSingleton())

//-----------------------------------------------------------------
// CharlieGame methods																				
//-----------------------------------------------------------------

CharlieGame::CharlieGame():m_BmpCharliePtr(nullptr), m_BmpLevel1Ptr(nullptr), m_CharliePtr(nullptr)
	,m_BmpBGPtr(nullptr)
	, m_CameraPos(0,-95)
	,m_ItemsTick(true)
	,m_HitLevelPtr(nullptr)
	,m_HitWaterPtr(nullptr)
	,m_HitPlatformPtr(nullptr)
	,m_animationCounter(0)
	,m_animationTick(0)
	,m_CoinCount(0)
	,m_Lifes(3)
	,m_DiamondCount(0)
	,m_Score(0)
	,m_HearthCount(3)
	,m_AudCratePtr(nullptr)
	,m_AudEnemyPtr(nullptr)
	,m_AudItemPtr(nullptr)
	,m_IsDead(false)
	,m_BmpFinishPtr(nullptr)
	,m_signTick(0)
	,m_signCounter(0)
	,m_TheEnd(false)
	,m_DrawEnd(false)
	,m_BmpGratsPtr(nullptr)
	,m_BmpGameOverPtr(nullptr)
	,m_GameOver(false)

{

	//Items
	for (int i = 0; i < ITEMS_BOX; i++)		m_BoxPtr[i]		=  nullptr;
	for (int i = 0; i < ITEMS_COIN; i++)	m_CoinPtr[i]	=  nullptr;
	for (int i = 0; i < ITEMS_DIAMOND; i++)	m_DiamondPtr[i] =  nullptr;
	for (int i = 0; i < ITEMS_HEART; i++)	m_HeartPtr[i]	=  nullptr;

	//HitRegions Items
	for (int i = 0; i < ITEMS_BOX; i++)		m_HitBoxPtr[i]		=  nullptr;
	for (int i = 0; i < ITEMS_COIN; i++)	m_HitCoinPtr[i]		=  nullptr;
	for (int i = 0; i < ITEMS_DIAMOND; i++)	m_HitDiamondPtr[i]  =  nullptr;
	for (int i = 0; i < ITEMS_HEART; i++)	m_HitHeartPtr[i]	=  nullptr;

	//Enemies
	for (int i = 0; i < UNITS_UNIT1; i++)	m_Unit1Ptr[i]	=  nullptr;
	for (int i = 0; i < UNITS_UNIT2; i++)	m_Unit2Ptr[i]	=  nullptr;
	for (int i = 0; i < UNITS_FROG; i++)	m_FrogPtr[i]	=  nullptr;

	//HitRegions Enemies
	for (int i = 0; i < UNITS_UNIT1; i++)	m_HitUnit1Ptr[i]	=  nullptr;
	for (int i = 0; i < UNITS_UNIT2; i++)	m_HitUnit2Ptr[i]	=  nullptr;
	for (int i = 0; i < UNITS_FROG; i++)	m_HitFrogPtr[i]		=  nullptr;

}

CharlieGame::~CharlieGame(){
	// nothing to destroy
}

void CharlieGame::GameInitialize(HINSTANCE hInstance){
	// Set the required values
	AbstractGame::GameInitialize(hInstance);
	GAME_ENGINE->SetTitle("Charlie The Duck - D'Heygere, R�my - 1DAE7");					
	GAME_ENGINE->RunGameLoop(true);

	// Set the optional values
	GAME_ENGINE->SetWidth(640);
	GAME_ENGINE->SetHeight(480);
	GAME_ENGINE->SetKeyList(String("OL") + (TCHAR) VK_SPACE + (TCHAR) VK_F5 + (TCHAR) VK_F9 );
	GAME_ENGINE->LockFramerateToVerticalSynchronisation(true);
}

void CharlieGame::GameStart(){

	//Bitmaps
	m_BmpCharliePtr = new Bitmap("./resources/charlie.bmp");

	m_BmpBGPtr = new Bitmap("./resources/bg.bmp");

	m_BmpLevel1Ptr = new Bitmap("./resources1/background.png");

	m_BmpWaterPtr = new Bitmap("./resources1/Water.bmp");
	m_BmpWaterPtr->SetTransparencyColor(255,0,255);

	m_BmpFinishPtr = new Bitmap("./resources1/End Level1.bmp");
	m_BmpFinishPtr->SetTransparencyColor(255,0,255);

	m_BmpGratsPtr = new Bitmap("./resources1/Congratulatons.png");
	m_BmpGratsPtr->SetTransparencyColor(255,255,255);

	m_BmpGameOverPtr = new Bitmap("./resources1/Game Over.png");


	//Hitregions
	m_HitLevelPtr = new HitRegion();
	m_HitLevelPtr->CreateFromSVG("./resources1/BackgroundNew.svg");

	m_HitWaterPtr = new HitRegion();
	m_HitWaterPtr->CreateFromSVG("./resources1/WaterHitBot.svg");

	m_HitPlatformPtr = new HitRegion();
	m_HitPlatformPtr->CreateFromSVG("./resources1/SemiPermeable.svg");


	//HUD
	m_BmpCharlieScore = new Bitmap("./resources1/HUD_txt.png");
	m_BmpCharlieScore->SetTransparencyColor(255,0,255);

	m_BmpScoreNr = new Bitmap("./resources1/HUD_numbers.png");
	m_BmpScoreNr->SetTransparencyColor(0,0,0);

	m_BmpIcons = new Bitmap("./resources1/HUD_icons1.png");
	m_BmpIcons->SetTransparencyColor(255,255,255);


	//Audio
	m_AudCratePtr  = new Audio("./resources1/Sounds/Crate.wav");
	m_AudEnemyPtr  = new Audio("./resources1/Sounds/Enemy.wav");
	m_AudItemPtr  = new Audio("./resources1/Sounds/Item.wav");

	//Default position if not overwritten by ReadFile()
	m_PosCharlie.x = 150;
	m_PosCharlie.y = 285;

	//Create Charlie
	DOUBLE2 Size;
	Size.x = m_BmpLevel1Ptr->GetWidth();
	Size.y = m_BmpLevel1Ptr->GetHeight();
	m_CharliePtr = new Charlie(m_PosCharlie, m_BmpCharliePtr, Size);

	//Read file
	ReadFile("CharlieLoad.txt", ',');
	//ReadFile("CharlieSave.txt",',');


}

void CharlieGame::GameEnd(){
	//Delete bitmaps
	delete m_BmpCharliePtr;
	delete m_BmpLevel1Ptr;
	delete m_BmpBGPtr;

	delete m_BmpWaterPtr;
	delete m_BmpFinishPtr;
	delete m_BmpGratsPtr;
	delete m_BmpGameOverPtr;

	delete m_BmpCharlieScore;
	delete m_BmpScoreNr;
	delete m_BmpIcons;

	//Delete
	delete m_CharliePtr;
	delete m_HitLevelPtr;
	delete m_HitWaterPtr;
	delete m_HitPlatformPtr;

	//Delete audio
	delete m_AudCratePtr,
	delete m_AudEnemyPtr,
	delete m_AudItemPtr;

	//Delete all Items' and Enemies' pointers and hitregions
	DeleteAll();
}

void CharlieGame::KeyPressed(TCHAR cKey){	
	
	if(!m_TheEnd)m_CharliePtr->KeyPressed(cKey);

	//quicksave F5 //quickload F9 //F-keys werken niet
	if(cKey == 'O' || cKey == VK_F5){ //Opslaan
		if(!m_TheEnd){
			WriteFile("CharlieSave.txt",',');
			OutputDebugString("Saved\n");
		}
		else OutputDebugString("Not a strategical place to save, eh?\n");
	}

	if(cKey == 'L' || cKey == VK_F9){ //Laden

		m_ItemsTick = false;
		m_TheEnd = false;
		m_DrawEnd = false;

		DeleteAll();

		ReadFile("CharlieSave.txt",',');
		//ReadFile("CharlieLoad.txt", ',');
		m_ItemsTick = true;
		m_GameOver = false;
		OutputDebugString("Loaded\n");
	}
}

void CharlieGame::GameTick(double deltaTime){
	m_CharliePtr->Tick(deltaTime);

	m_AudCratePtr->Tick();
	m_AudEnemyPtr->Tick();
	m_AudItemPtr->Tick();

	if(m_ItemsTick){ //If pointers aren't being loaded or deleted
		for (int i = 0; i < ITEMS_BOX; i++) 	if(m_BoxPtr[i]!=nullptr) m_BoxPtr[i]->Tick();
		for (int i = 0; i < ITEMS_COIN; i++)	if(m_CoinPtr[i]!=nullptr) m_CoinPtr[i]->Tick();
		for (int i = 0; i < ITEMS_DIAMOND; i++)	if(m_DiamondPtr[i]!=nullptr) m_DiamondPtr[i]->Tick();
		for (int i = 0; i < ITEMS_HEART; i++)	if(m_HeartPtr[i]!=nullptr)  m_HeartPtr[i]->Tick();



		for (int i = 0; i < UNITS_UNIT1; i++)	if(m_Unit1Ptr[i]!=nullptr)	 m_Unit1Ptr[i]->Tick(deltaTime);
		for (int i = 0; i < UNITS_UNIT2; i++)	if(m_Unit2Ptr[i]!=nullptr)	 m_Unit2Ptr[i]->Tick(deltaTime);
		for (int i = 0; i < UNITS_FROG; i++)	if(m_FrogPtr[i]!=nullptr)	 m_FrogPtr[i]->Tick(deltaTime);
	}
	
	if(!m_IsDead && !m_TheEnd){
		if (!m_CharliePtr->SwimTest(deltaTime, m_HitWaterPtr)){
			m_CharliePtr->OnPlatform(deltaTime, m_HitPlatformPtr);
			m_CharliePtr->MoveCharlie(deltaTime, m_HitLevelPtr);
		}
	}
	else if(m_IsDead && m_CharliePtr->Die(deltaTime)){
		if(m_Lifes > 0){
			m_HearthCount = 3;
			DeleteAll();
			m_IsDead = false;
			ReadFile("CharlieLoad.txt", ','); //reset
		}
		else m_GameOver = true;
	}
	else if (m_TheEnd && m_CharliePtr->MoveAlong(deltaTime)){
		m_DrawEnd = true;
	}

	//Water animation
	if(m_animationCounter == 0){
		++m_animationTick;
		m_animationTick = m_animationTick % m_BmpWaterPtr->GetWidth();
	}

	++m_animationCounter;
	m_animationCounter = m_animationCounter % WATER_CLIP;

	//Finish sign animation
	if (m_signCounter ==0){
		++m_signTick;
		m_signTick = m_signTick % SIGN_CLIP;
	}

	++m_signCounter;
	m_signCounter = m_signCounter % 6;

	if(m_ItemsTick){ //DO NOT TOUCH!!! Preventing threading issues  //If pointers aren't being loaded or deleted
		ProcessHitRegions();
		ProcessCollisionsCharlie();
		KillOffscreen();
		EndGame();
	}
}

void CharlieGame::GamePaint(RECT rect){
	GAME_ENGINE->DrawSolidBackground(69,95,157);
	m_CameraPos = m_CharliePtr->GetPosCam();
	MATRIX3X2 matCamera, matCamPos, matCamCenter, matTranslate, matTransform;
	
	//Set camera
	matCamPos.SetAsTranslate(m_CameraPos.x, m_CameraPos.y);
	matCamCenter.SetAsTranslate(-GAME_ENGINE->GetWidth()/2, -GAME_ENGINE->GetHeight()/2);
	matCamera = matCamCenter * matCamPos * matCamCenter.Inverse();
	MATRIX3X2 matView = matCamera.Inverse();
	GAME_ENGINE->SetTransformMatrix(matView);
	GAME_ENGINE->DrawBitmap(m_BmpLevel1Ptr);

	if(m_ItemsTick){ //If pointers aren't being loaded or deleted
		for (int i = 0; i < ITEMS_BOX; i++) 	if(m_BoxPtr[i]!=nullptr) m_BoxPtr[i]->Paint();
		for (int i = 0; i < ITEMS_COIN; i++)	if(m_CoinPtr[i]!=nullptr) m_CoinPtr[i]->Paint();
		for (int i = 0; i < ITEMS_DIAMOND; i++)	if(m_DiamondPtr[i]!=nullptr) m_DiamondPtr[i]->Paint();
		for (int i = 0; i < ITEMS_HEART; i++)	if(m_HeartPtr[i]!=nullptr)  m_HeartPtr[i]->Paint();

		for (int i = 0; i < UNITS_UNIT1; i++)	if(m_Unit1Ptr[i]!=nullptr)	 m_Unit1Ptr[i]->Paint();
		for (int i = 0; i < UNITS_UNIT2; i++)	if(m_Unit2Ptr[i]!=nullptr)	 m_Unit2Ptr[i]->Paint();
		for (int i = 0; i < UNITS_FROG; i++)	if(m_FrogPtr[i]!=nullptr)	 m_FrogPtr[i]->Paint();
	}
	
	GAME_ENGINE->SetColor(COLOR(255,0,0));
	m_CharliePtr->Paint();
	RECT2 hitBody = m_CharliePtr->GetActorVPtr();

	GAME_ENGINE->SetTransformMatrix(matView);
	DrawWater(1277, 522, 120);
	DrawWater(2633, 522, 200);
	DrawWater(4070, 522, 120);
	DrawWater(4310, 522, 117);

	DrawFinish();
	//DrawHitRegions();

	MATRIX3X2 matRegular;
	GAME_ENGINE->SetTransformMatrix(matRegular);

	PaintHUD();

	if(m_DrawEnd){
		int x1 = GAME_ENGINE->GetWidth()/2;
		int y1 = GAME_ENGINE->GetHeight()/2;
		int x2 = m_BmpGratsPtr->GetWidth()/2;
		int y2 = m_BmpGratsPtr->GetHeight()/2;

		GAME_ENGINE->DrawBitmap(m_BmpGratsPtr, x1 - x2, y1 - y2);
	}
	else if (m_GameOver){
		GAME_ENGINE->DrawSolidBackground(0,0,0);
		GAME_ENGINE->DrawBitmap(m_BmpGameOverPtr, 5, -50);
	}
}

void CharlieGame::WriteFile(std::string fileName, char sep){
	std::ofstream file(fileName);

	m_PosCharlie = m_CharliePtr->GetCharliePos();
	m_CameraPos = m_CharliePtr->GetPosCam();

	if (!file)
		std::cout << "Error when creating the file." << std::endl;
	else {
		file << "[Charlie]" << std::endl;
		file << "CharlieSpawnPositionX=" << m_PosCharlie.x << std::endl;
		file << "CharlieSpawnPositionY=" << m_PosCharlie.y << std::endl;
		file << "CameraPosX=" << m_CameraPos.x << std::endl;
		file << "CameraPosY=" << m_CameraPos.y << std::endl;
		file << "Coins=" << m_CoinCount << std::endl;
		file << "Lifes=" << m_Lifes << std::endl;
		file << "Hearts=" << m_HearthCount << std::endl;
		file << "Diamonds=" << m_DiamondCount << std::endl;
		file << "Score=" << m_Score << std::endl;
		//Items
		file << "" << std::endl;
		file << "[CoinPos]" << std::endl;
		DOUBLE2 position;
		for (int i = 0; i < ITEMS_COIN; i++)
		{
			if(m_CoinPtr[i] != nullptr){ 
				position = m_CoinPtr[i]->GetPos();	
				if ( m_CoinPtr[i]->GetMovement()) file << "Coin=" << position.x << ',' << position.y << " (true)" << std::endl;
				else file << "Coin=" << position.x << ',' << position.y << std::endl;
			}
		}

		file << "" << std::endl;
		file << "[DiamondPos]" << std::endl;
		for (int i = 0; i < ITEMS_DIAMOND; i++)
		{
			if(m_DiamondPtr[i] != nullptr){
				position = m_DiamondPtr[i]->GetPos();	
				if ( m_DiamondPtr[i]->GetMovement()) file << "Diamond=" << position.x << ',' << position.y << " (true)" << std::endl;
				else file << "Diamond=" << position.x << ',' << position.y << std::endl;
			}
		}

		file << "" << std::endl;
		file << "[BoxPos]" << std::endl;
		for (int i = 0; i < ITEMS_BOX; i++)
		{
			if(m_BoxPtr[i] != nullptr){
				position = m_BoxPtr[i]->GetPos();	
				int itemID = m_BoxPtr[i]->GetItemInside();
				std::string item = "";

				switch (itemID){
				case COIN_ID: item = " (coin)";
					break;
				case DIAMOND_ID: item = " (diamond)";
					break;
				case HEART_ID: item = " (heart)";
					break;
				}

				file << "Box=" << position.x << ',' << position.y << item << std::endl;
			}
		}

		//Enemies
		int endPos;
		file << "" << std::endl;
		file << "[Unit1]" << std::endl;
		for (int i = 0; i < UNITS_UNIT1; i++)
		{
			if(m_Unit1Ptr[i] != nullptr){
				position = m_Unit1Ptr[i]->GetPos();
				endPos =	m_Unit1Ptr[i]->GetEndPos();
				file << "startPosU1=" << (int)position.x << ',' << position.y << ";endPosU1=" << endPos << std::endl;
			}
		}

		file << "" << std::endl;
		file << "[Unit2]" << std::endl;
		for (int i = 0; i < UNITS_UNIT2; i++)
		{
			if(m_Unit2Ptr[i] != nullptr){
				position = m_Unit2Ptr[i]->GetPos();
				endPos =	m_Unit2Ptr[i]->GetEndPos();
				file << "startPosU2=" << (int)position.x << ',' << position.y << ";endPosU2=" << endPos << std::endl;
			}
		}

		file << "" << std::endl;
		file << "[Frog]" << std::endl;
		for (int i = 0; i < UNITS_UNIT2; i++)
		{
			if(m_FrogPtr[i] != nullptr){
				position = m_FrogPtr[i]->GetPos();
				endPos =	m_FrogPtr[i]->GetEndPos();
				file << "startPosFrog=" << (int)position.x << ',' << position.y << ";endPosFrog=" << endPos << std::endl;
			}
		}

		file << "" << std::endl;
		file << "[Bee]" << std::endl;
		for (int i = 0; i < UNITS_UNIT2; i++)
		{
			if(m_FrogPtr[i] != nullptr){
				position = m_FrogPtr[i]->GetPos();
				endPos =	m_FrogPtr[i]->GetEndPos();
				file << "startPosBee=" << (int)position.x << ',' << position.y << ";" <<std::endl;
			}
		}
	}
}

void CharlieGame::ReadFile(std::string fileName, char sep){
	std::ifstream file(fileName);

	if (!file)
		std::cout << "Could not open the file " << fileName << std::endl;
	else {
		std::string sLine;
		int lineCount=0;
		do {
			getline(file, sLine);
			ProcessLine(sLine,sep,lineCount);
			++lineCount;
		} while (!file.eof());
		file.close();
	}
}

void CharlieGame::ProcessLine(std::string line, char sep, int shapeIndex){
	int  x=0, y=0;
	DOUBLE2 cam;
	//Find string and save r-value
	if(!(line.find("CharlieSpawnPositionX="))) x = atoi(line.substr(line.find("=") +1).c_str());
	if(!(line.find("CharlieSpawnPositionY="))) y = atoi(line.substr(line.find("=") +1).c_str());

	if(!(line.find("CameraPosX="))) cam.x = atof(line.substr(line.find("=") +1).c_str());
	else cam.x = -1;
	if(!(line.find("CameraPosY="))) cam.y = atof(line.substr(line.find("=") +1).c_str());
	else cam.y = -1;

	if(!(line.find("Coins="))) m_CoinCount = atoi(line.substr(line.find("=") +1).c_str());
	if(!(line.find("Lifes="))) m_Lifes = atoi(line.substr(line.find("=") +1).c_str());
	if(!(line.find("Hearts="))) m_HearthCount = atoi(line.substr(line.find("=") +1).c_str());
	if(!(line.find("Diamonds="))) m_DiamondCount = atoi(line.substr(line.find("=") +1).c_str());
	if(!(line.find("Score="))) m_Score = atoi(line.substr(line.find("=") +1).c_str());

	if (x != 0){
		m_PosCharlie.x = x;
		m_CharliePtr->SetCharliePos(m_PosCharlie);
	}
	if (y != 0){
		m_PosCharlie.y = y;
		m_CharliePtr->SetCharliePos(m_PosCharlie);
	}
	if (cam.x != -1){
		m_CameraPos.x = cam.x;
		m_CharliePtr->SetPosCam(m_CameraPos);
	}
	if (cam.y != -1){
		m_CameraPos.y = cam.y;
		m_CharliePtr->SetPosCam(m_CameraPos);
	}

	//Find and set Item and Enemy values
	ProcessItem(line, sep);
	ProcessEnemy(line, sep);
}

void CharlieGame::CreateItem(int ItemID, int PosX, int PosY, bool movement){
	DOUBLE2 pos(PosX, PosY);
	int arrPos = -1;
	switch (ItemID)
	{
	case BOX_ID: //Seperate function
		OutputDebugString("Wrong place");
		break;
	case COIN_ID:
		for (int i = 0; i < ITEMS_COIN; i++){
			if (m_CoinPtr[i] == nullptr){
				arrPos = i;
				break;
			}
		}
		if(arrPos >= 0){
			m_CoinPtr[arrPos] = new Coin(pos, ItemID, movement);
			m_HitCoinPtr[arrPos] = new HitRegion();
			m_HitCoinPtr[arrPos]->CreateFromRect(0, 0, 32, 32);
		}
		break;
	case DIAMOND_ID:
		for (int i = 0; i < ITEMS_DIAMOND; i++){
			if (m_DiamondPtr[i] == nullptr){
				arrPos = i;
				break;
			}
		}
		if(arrPos >= 0){
			m_DiamondPtr[arrPos] = new Diamond(pos, ItemID, movement);
			m_HitDiamondPtr[arrPos] = new HitRegion();
			m_HitDiamondPtr[arrPos]->CreateFromRect(0, 0, 30, 30);
		}
		break;
	case HEART_ID:
		for (int i = 0; i < ITEMS_HEART; i++){
			if (m_HeartPtr[i] == nullptr){
				arrPos = i;
				break;
			}
		}
		if(arrPos >= 0){
			m_HeartPtr[arrPos] = new Heart(pos, ItemID, movement);
			m_HitHeartPtr[arrPos] = new HitRegion();
			m_HitHeartPtr[arrPos]->CreateFromRect(0, 0, 30, 28);
		}
		break;
	}
}

void CharlieGame::CreateBox(int PosX, int PosY, int ItemIDInside){
	DOUBLE2 pos(PosX, PosY);

	for (int i = 0; i < ITEMS_BOX; i++){
		if (m_BoxPtr[i] == nullptr) {
			m_BoxPtr[i] = new Box(pos, BOX_ID, ItemIDInside);
			m_HitBoxPtr[i] = new HitRegion();
			m_HitBoxPtr[i]->CreateFromRect(0, 0, 42, 33);
			break;
		}
	}
}

void CharlieGame::CreateEnemy(int startPosX, int startPosY, int endPosX, int enemyID){
	DOUBLE2 pos(startPosX, startPosY);
	DOUBLE2 endPos(endPosX, startPosY);
	int arrPos = -1;
	switch (enemyID)
	{
	case UNIT1_ID:
		for (int i = 0; i < UNITS_UNIT1; i++){
			if (m_Unit1Ptr[i] == nullptr){
				arrPos = i;
				break;
			}
		}
		if(arrPos >= 0){
			m_Unit1Ptr[arrPos] = new Unit1(pos, enemyID, endPos);
			m_HitUnit1Ptr[arrPos] = new HitRegion();
			m_HitUnit1Ptr[arrPos]->CreateFromRect(0, 0, 44, 33);
		}
		break;
	case UNIT2_ID:
		arrPos = -1;
		for (int i = 0; i < UNITS_UNIT2; i++){
			if (m_Unit2Ptr[i] == nullptr){
				arrPos = i;
				break;
			}
		}
		if(arrPos >= 0){
			m_Unit2Ptr[arrPos] = new Unit2(pos, enemyID, endPos);
			m_HitUnit2Ptr[arrPos] = new HitRegion();
			m_HitUnit2Ptr[arrPos]->CreateFromRect(0, 0, 44, 33);
		}
		break;
	case FROG_ID:
		arrPos = -1;
		for (int i = 0; i < UNITS_FROG; i++){
			if (m_FrogPtr[i] == nullptr){
				arrPos = i;
				break;
			}
		}
		if(arrPos >= 0){
			m_FrogPtr[arrPos] = new Frog(pos, enemyID, endPos);
			m_HitFrogPtr[arrPos] = new HitRegion();
			m_HitFrogPtr[arrPos]->CreateFromRect(0, 0, 42, 42);
		}
		break;
	}
}

void CharlieGame::CreateBee(int startPosX, int startPosY){
	
}

void CharlieGame::DrawWater(int posX, int posY, int length){

	if(m_BmpWaterPtr->GetWidth() < m_animationTick * WATER_CLIP + length) m_animationTick = 0;
	RECT water;
	water.left = m_animationTick * WATER_CLIP;
	water.right = water.left + length;
	water.top = 0;
	water.bottom = m_BmpWaterPtr->GetHeight();

	GAME_ENGINE->DrawBitmap(m_BmpWaterPtr,posX, posY, water);
}

//Find Items in File
void CharlieGame::ProcessItem(std::string line, char sep){
	if(!(line.find("Coin="))){
		int x = atoi(line.substr((line.find("=") +1),sep).c_str());
		int startPos =line.find(sep)+1;
		std::string yPos = line.substr(startPos,std::string::npos);
		int y = atoi(yPos.c_str());
		bool movement = false;
		if(line.find("(true)") != -1) movement = true;

		CreateItem(COIN_ID, x, y, movement);
	}

	if(!(line.find("Diamond="))){
		int x = atoi(line.substr((line.find("=") +1),sep).c_str());
		int startPos =line.find(sep)+1;
		std::string yPos = line.substr(startPos,std::string::npos);
		int y = atoi(yPos.c_str());
		bool movement = false;
		if(line.find("(true)") != -1) movement = true;

		CreateItem(DIAMOND_ID, x, y, movement);
	}

	if(!(line.find("Box="))){
		int x = atoi(line.substr((line.find("=") +1),sep).c_str());
		int startPos =line.find(sep)+1;
		std::string yPos = line.substr(startPos,std::string::npos);
		int y = atoi(yPos.c_str());

		int ITEM_ID = 0;
		if(line.find("(coin)") != -1)ITEM_ID = COIN_ID;
		if(line.find("(diamond)") != -1)ITEM_ID = DIAMOND_ID;
		if(line.find("(heart)") != -1)ITEM_ID = HEART_ID;

		CreateBox(x, y, ITEM_ID);
	}
}
//Find Enemies in File
void CharlieGame::ProcessEnemy(std::string line, char sep){

	if(!(line.find("startPosU1="))){

		int x = atoi(line.substr((line.find("=") +1),sep).c_str());

		int startPos =line.find(sep)+1;
		std::string yPos = line.substr(startPos,';');
		int y = atoi(yPos.c_str());

		startPos = line.find(";")+1;
		std::string x2Pos = line.substr(startPos,std::string::npos);
		int x2 = atoi(x2Pos.substr((x2Pos.find("=") +1),std::string::npos).c_str());

		CreateEnemy(x, y, x2, UNIT1_ID);
	}

	if(!(line.find("startPosU2="))){

		int x = atoi(line.substr((line.find("=") +1),sep).c_str());

		int startPos =line.find(sep)+1;
		std::string yPos = line.substr(startPos,';');
		int y = atoi(yPos.c_str());

		startPos = line.find(";")+1;
		std::string x2Pos = line.substr(startPos,std::string::npos);
		int x2 = atoi(x2Pos.substr((x2Pos.find("=") +1),std::string::npos).c_str());

		CreateEnemy(x, y, x2, UNIT2_ID);
	}

	if(!(line.find("startPosFrog="))){
		int x = atoi(line.substr((line.find("=") +1),sep).c_str());

		int startPos =line.find(sep)+1;
		std::string yPos = line.substr(startPos,';');
		int y = atoi(yPos.c_str());

		startPos = line.find(";")+1;
		std::string x2Pos = line.substr(startPos,std::string::npos);
		int x2 = atoi(x2Pos.substr((x2Pos.find("=") +1),std::string::npos).c_str());

		CreateEnemy(x, y, x2, FROG_ID);
	}

	if(!(line.find("startPosBee="))){
		int x = atoi(line.substr((line.find("=") +1),sep).c_str());

		int startPos =line.find(sep)+1;
		std::string yPos = line.substr(startPos,';');
		int y = atoi(yPos.c_str());

		CreateBee(x, y);
	}
}

void CharlieGame::PositionHitRegions(HitRegion *hitRegion[], int arrLength, int ID, bool isItem){
	arrLength += 1;
	DOUBLE2 arrPos[ITEMS];
	if (isItem){
		switch (ID){
		case BOX_ID:
			for (int i = 0; i < arrLength; i++) if( m_BoxPtr[i] != nullptr) arrPos[i] = m_BoxPtr[i]->GetPos();
			break;

		case COIN_ID:
			for (int i = 0; i < arrLength; i++)	if( m_CoinPtr[i] != nullptr) arrPos[i] = m_CoinPtr[i]->GetPos();
			break;

		case DIAMOND_ID:
			for (int i = 0; i < arrLength; i++)	if( m_DiamondPtr[i] != nullptr) arrPos[i] = m_DiamondPtr[i]->GetPos();
			break;

		case HEART_ID:
			for (int i = 0; i < arrLength; i++)	if( m_HeartPtr[i] != nullptr) arrPos[i] = m_HeartPtr[i]->GetPos();
			break;
		default:
			break;
		}
	}

	else {
		switch (ID){
		case UNIT1_ID:
			for (int i = 0; i < arrLength; i++)	if( m_Unit1Ptr[i] != nullptr) arrPos[i] = m_Unit1Ptr[i]->GetPos();
			break;

		case UNIT2_ID:
			for (int i = 0; i < arrLength; i++)	if( m_Unit2Ptr[i] != nullptr) arrPos[i] = m_Unit2Ptr[i]->GetPos();
			break;

		case FROG_ID:
			for (int i = 0; i < arrLength; i++)	if( m_FrogPtr[i] != nullptr) arrPos[i] = m_FrogPtr[i]->GetPos();
			break;

			//	case BEE_ID:
			//for (int i = 0; i < arrLength; i++)	arrPos[i] = m_BeePtr[i]->GetPos();
			//		break;
		default:
			break;
		}
	}


	for (int i = 0; i < arrLength; i++){
		if(arrPos[i].x != 0 && arrPos[i].y != 0 && hitRegion[i] != nullptr) hitRegion[i]->SetPos(arrPos[i]);
	}
}

void CharlieGame::ProcessHitRegions(){
	int arrLenght = -1;

	//ITEMS
	//Box
	for (int i = 0; i < ITEMS_BOX; i++){
		if (m_BoxPtr[i] != nullptr) arrLenght = i;
	}
	if (arrLenght >= 0) PositionHitRegions(m_HitBoxPtr, arrLenght, BOX_ID, true);

	//Coin
	arrLenght = -1;
	for (int i = 0; i < ITEMS_COIN; i++){
		if (m_CoinPtr[i] != nullptr) arrLenght = i;
	}
	if (arrLenght >= 0) PositionHitRegions(m_HitCoinPtr, arrLenght, COIN_ID, true);

	//Diamond
	arrLenght = -1;
	for (int i = 0; i < ITEMS_DIAMOND; i++){
		if (m_DiamondPtr[i] != nullptr) arrLenght = i;
	}
	if (arrLenght >= 0) PositionHitRegions(m_HitDiamondPtr, arrLenght, DIAMOND_ID, true);

	//Heart
	arrLenght = -1;
	for (int i = 0; i < ITEMS_HEART; i++){
		if (m_HeartPtr[i] != nullptr) arrLenght = i;
	}
	if (arrLenght >= 0) PositionHitRegions(m_HitHeartPtr, arrLenght, HEART_ID, true);


	//ENEMIES
	//Unit1
	for (int i = 0; i < UNITS_UNIT1; i++){
		if (m_Unit1Ptr[i] != nullptr) arrLenght = i;
	}
	if (arrLenght >= 0) PositionHitRegions(m_HitUnit1Ptr, arrLenght, UNIT1_ID, false);

	//Unit2
	arrLenght = -1;
	for (int i = 0; i < UNITS_UNIT2; i++){
		if (m_Unit2Ptr[i] != nullptr) arrLenght = i;
	}
	if (arrLenght >= 0) PositionHitRegions(m_HitUnit2Ptr, arrLenght, UNIT2_ID, false);

	//Frog
	arrLenght = -1;
	for (int i = 0; i < UNITS_FROG; i++){
		if (m_FrogPtr[i] != nullptr) arrLenght = i;
	}
	if (arrLenght >= 0) PositionHitRegions(m_HitFrogPtr, arrLenght, FROG_ID, false);

	//Bee
}

void CharlieGame::DrawHitRegions(){
	//Items
	for (int i = 0; i < ITEMS_BOX; i++)		if(m_HitBoxPtr[i]!=nullptr)		GAME_ENGINE->DrawHitRegion(m_HitBoxPtr[i]);
	for (int i = 0; i < ITEMS_COIN; i++)	if(m_HitCoinPtr[i]!=nullptr)	GAME_ENGINE->DrawHitRegion(m_HitCoinPtr[i]);
	for (int i = 0; i < ITEMS_DIAMOND; i++)	if(m_HitDiamondPtr[i]!=nullptr)	GAME_ENGINE->DrawHitRegion(m_HitDiamondPtr[i]);
	for (int i = 0; i < ITEMS_HEART; i++)	if(m_HitHeartPtr[i]!=nullptr)	GAME_ENGINE->DrawHitRegion(m_HitHeartPtr[i]);

	//Enemies
	for (int i = 0; i < UNITS_UNIT1 + 1; i++) if(m_HitUnit1Ptr[i]!=nullptr)	GAME_ENGINE->DrawHitRegion(m_HitUnit1Ptr[i]);
	for (int i = 0; i < UNITS_UNIT2; i++)	  if(m_HitUnit2Ptr[i]!=nullptr)	GAME_ENGINE->DrawHitRegion(m_HitUnit2Ptr[i]);
	for (int i = 0; i < UNITS_FROG; i++)	  if(m_HitFrogPtr[i]!=nullptr) 	GAME_ENGINE->DrawHitRegion(m_HitFrogPtr[i]);
}

bool CharlieGame::CollisionCharlie(RECT2 hitRect2){
	m_CharlieHit = CharlieHit::NONE;

	RECT2 hitFeet = m_CharliePtr->GetActorHPtr();
	RECT2 hitBody = m_CharliePtr->GetActorVPtr();

	if(abs(hitRect2.top - hitFeet.bottom) < 10 && m_CharliePtr->IsFalling())
		m_CharlieHit = CharlieHit::BOTTOM;
	if(abs(hitRect2.right - hitBody.left) < 2) m_CharlieHit = CharlieHit::LEFT;
	if(abs(hitRect2.left - hitBody.right) < 2) m_CharlieHit = CharlieHit::RIGHT;
	if(abs(hitRect2.bottom - hitBody.top) < 2 && !m_CharliePtr->IsFalling()) m_CharlieHit = CharlieHit::TOP;

	bool noIntersect=
		hitRect2.right < hitBody.left || hitRect2.left > hitBody.right || 
		hitRect2.top > hitBody.bottom || hitRect2.bottom < hitBody.top;
	return !noIntersect;
}

void CharlieGame::ProcessCollisionsCharlie(){
	if (!m_IsDead){
		//Items
		//Box
		for (int i = 0; i < ITEMS_BOX; i++){
			if (m_HitBoxPtr[i] != nullptr){
				if(CollisionCharlie(m_HitBoxPtr[i]->GetBounds())){

					switch (m_CharlieHit)
					{				
					case CharlieGame::CharlieHit::BOTTOM:
						m_CharliePtr->Bounce();
						ItemPickedUp(m_HitBoxPtr, BOX_ID, i);
						break;
					case CharlieGame::CharlieHit::TOP:
						m_CharliePtr->BumpHead();
						break;
					case CharlieGame::CharlieHit::LEFT:
						m_CharliePtr->BlockedLeft();
						break;
					case CharlieGame::CharlieHit::RIGHT:
						m_CharliePtr->BlockedRight();
						break;
					default:
						break;

					}				
				}
			}
		}

		//Coin
		ProcessItem(ITEMS_COIN, COIN_ID, m_HitCoinPtr);
		//Diamond
		ProcessItem(ITEMS_DIAMOND, DIAMOND_ID, m_HitDiamondPtr);
		//Heart
		ProcessItem(ITEMS_HEART, HEART_ID, m_HitHeartPtr);

		//Enemies
		//Unit1
		ProcessEnemy(UNITS_UNIT1, UNIT1_ID, m_HitUnit1Ptr);
		//Unit2
		ProcessEnemy(UNITS_UNIT2, UNIT2_ID, m_HitUnit2Ptr);
		//Frog
		ProcessEnemy(UNITS_FROG, FROG_ID, m_HitFrogPtr);

	}
}

void CharlieGame::ProcessItem(const int arrLength, const int ID, HitRegion *hitRegion[]){
	for (int i = 0; i < arrLength; i++){
		if (hitRegion[i] != nullptr){
			if(CollisionCharlie(hitRegion[i]->GetBounds())){
				ItemPickedUp(hitRegion, ID, i);
			}
		}
	}
}

void CharlieGame::ProcessEnemy(const int arrLength, const int ID, HitRegion *hitRegion[]){
	for (int i = 0; i < arrLength; i++){
		if (hitRegion[i] != nullptr){
			if(CollisionCharlie(hitRegion[i]->GetBounds())){
				if (m_CharlieHit == CharlieGame::CharlieHit::BOTTOM){
					m_CharliePtr->Bounce();
					//	Unit DIE
					KillEnemy(hitRegion, ID, i);
				}
				else{
					//Life--;
					if(m_CharliePtr->Hit()){
						if(m_HearthCount > 0)--m_HearthCount;
						else Die();
					}
				}
			}
		}
	}
}

void CharlieGame::ItemPickedUp(HitRegion *hitRegion[], int itemID, int arrPos){
	DOUBLE2 pos;
	int id = -1;
	delete hitRegion[arrPos];
	hitRegion[arrPos] = nullptr;

	switch (itemID){
	case BOX_ID:
		pos = m_BoxPtr[arrPos]->GetPos();
		id = m_BoxPtr[arrPos]->GetItemInside();
		CreateItem(id, (int) pos.x, (int) pos.y, false);
		delete m_BoxPtr[arrPos];
		m_BoxPtr[arrPos] = nullptr;
		m_Score += 10;
		m_AudCratePtr->Play();
		break;

	case COIN_ID:
		delete m_CoinPtr[arrPos];
		m_CoinPtr[arrPos] = nullptr;
		++m_CoinCount;
		m_Score += 50;
		m_AudItemPtr->Play();
		break;

	case DIAMOND_ID:
		delete m_DiamondPtr[arrPos];
		m_DiamondPtr[arrPos] = nullptr;
		++m_DiamondCount;
		m_Score += 100;
		m_AudItemPtr->Play();
		break;

	case HEART_ID:
		delete m_HeartPtr[arrPos];
		m_HeartPtr[arrPos] = nullptr;
		if(m_HearthCount < 3) ++m_HearthCount;
		m_Score += 500;
		m_AudItemPtr->Play();
		break;

	}
}

void CharlieGame::DeleteAll(){
	//Items
	DeletePtr(ITEMS_BOX, m_HitBoxPtr);
	DeletePtr(ITEMS_BOX, m_BoxPtr);

	DeletePtr(ITEMS_COIN, m_HitCoinPtr);
	DeletePtr(ITEMS_COIN, m_CoinPtr);

	DeletePtr(ITEMS_DIAMOND, m_HitDiamondPtr);
	DeletePtr(ITEMS_DIAMOND, m_DiamondPtr);

	DeletePtr(ITEMS_HEART, m_HitHeartPtr);
	DeletePtr(ITEMS_HEART, m_HeartPtr);

	//Enemies
	DeletePtr(UNITS_UNIT1, m_HitUnit1Ptr);
	DeletePtr(UNITS_UNIT1, m_Unit1Ptr);

	DeletePtr(UNITS_UNIT2, m_HitUnit2Ptr);
	DeletePtr(UNITS_UNIT2, m_Unit2Ptr);

	DeletePtr(UNITS_FROG, m_HitFrogPtr);
	DeletePtr(UNITS_FROG, m_FrogPtr);
	//Bee
}

void CharlieGame::KillEnemy(HitRegion *hitRegion[], int enemyID, int arrPos){
	delete hitRegion[arrPos];
	hitRegion[arrPos] = nullptr;

	OutputDebugString(String("Enemy killed: ") + enemyID + "/n");
	switch (enemyID){
	case UNIT1_ID:
		if(m_Unit1Ptr[arrPos] != nullptr) m_Unit1Ptr[arrPos]->Kill();
		m_Score += 100;
		m_AudEnemyPtr->Play();
		break;

	case UNIT2_ID:
		if(m_Unit2Ptr[arrPos] != nullptr) m_Unit2Ptr[arrPos]->Kill();
		m_Score += 100;
		m_AudEnemyPtr->Play();
		break;

	case FROG_ID:
		if(m_FrogPtr[arrPos] != nullptr) m_FrogPtr[arrPos]->Kill();
		m_Score += 100;
		m_AudEnemyPtr->Play();
		break;

		/*case BEE_ID:
		delete m_HeartPtr[arrPos];
		m_HeartPtr[arrPos] = nullptr;
		break;*/

	}

}

void CharlieGame::KillOffscreen(){
	if(m_BmpLevel1Ptr != nullptr){
		CheckOffscreen(UNITS_UNIT1, m_Unit1Ptr);
		CheckOffscreen(UNITS_UNIT2, m_Unit2Ptr);
		CheckOffscreen(UNITS_FROG, m_FrogPtr);
		//Bee
	}
}

void CharlieGame::PaintHUD(){
	RECT HUDRect;
	HUDRect.left = 0;
	HUDRect.right = m_BmpCharlieScore->GetWidth()/2;
	HUDRect.top = 0;
	HUDRect.bottom = m_BmpCharlieScore->GetHeight();
	GAME_ENGINE->DrawBitmap(m_BmpCharlieScore, 10, 20, HUDRect); //"CHARLIE"

	HUDRect.left = m_BmpCharlieScore->GetWidth()/2;
	HUDRect.right = m_BmpCharlieScore->GetWidth();
	GAME_ENGINE->DrawBitmap(m_BmpCharlieScore, 470, 20, HUDRect); //"SCORE"


	PaintIcon(2, 80, 18);//"X"

	PaintIcon(4, 165, 15);//Diamond icon
	PaintIcon(2, 190, 18);//"X"

	PaintIcon(5, 265, 15);//Coin icon
	PaintIcon(2, 292, 18);//"X"

	PaintIcon(m_HearthCount >= 1 ? 1 : 0, 365, 15);//Hearth icon
	PaintIcon(m_HearthCount >= 2 ? 1 : 0, 389, 15);//Hearth icon
	PaintIcon(m_HearthCount == 3 ? 1 : 0, 414, 15);//Hearth icon

	PaintIcon(3, 525, 18);//":"

	PaintNumber(m_Lifes, 110, 20);
	PaintNumber(m_DiamondCount, 220, 20);
	PaintNumber(m_CoinCount, 320, 20);
	PaintNumber(m_Score, 542, 20);
}

void CharlieGame::PaintNumber(int number, int posX, int posY){
	RECT numberRect;
	int offset = 0;
	numberRect.top = 0;
	numberRect.bottom = SCORE_HEIGHT;

	std::string strScore;
	std::ostringstream convert;
	convert << number;
	strScore = convert.str();

	int *arrNr = new int[strScore.size()];
	for(size_t i = 0; i < strScore.size(); ++i)
	{
		arrNr[i] = strScore[i] - '0';
		if(arrNr[i] >= 0){
			numberRect.left = arrNr[i] * SCORE_CLIP_WIDTH;
			numberRect.right = numberRect.left + SCORE_CLIP_WIDTH;
			GAME_ENGINE->DrawBitmap(m_BmpScoreNr, posX + offset, posY, numberRect);
			offset += SCORE_CLIP_WIDTH;
		}
	}
	delete arrNr;

}

void CharlieGame::PaintIcon(int iconPos, int posX, int posY){
	RECT HUDRect;
	HUDRect.top = 0;
	HUDRect.bottom = ICON_HEIGHT;
	HUDRect.left = ICON_CLIP_WIDTH * iconPos;
	HUDRect.right = HUDRect.left + ICON_CLIP_WIDTH;
	GAME_ENGINE->DrawBitmap(m_BmpIcons, posX, posY, HUDRect);
}

void CharlieGame::Die(){
	m_IsDead = true;
	if(m_Lifes == 0) m_GameOver = true;
	else --m_Lifes;
}

void CharlieGame::EndGame(){
	double width = m_BmpFinishPtr->GetWidth()/9;
	int height = m_BmpFinishPtr->GetHeight();

	int posX = 6000, posY = 283;
	RECT sign;
	sign.left = m_signTick * (long)width;
	sign.right = sign.left + (long)width;
	sign.top = 0;
	sign.bottom = height;

	RECT2 hitTest(sign.left + posX, sign.top + posY, sign.right + posX, sign.bottom + posY);

	if(CollisionCharlie(hitTest)){
		m_TheEnd = true;
	}
}

void CharlieGame::DrawFinish(){
	double width = m_BmpFinishPtr->GetWidth()/9;

	RECT sign;
	sign.left = m_signTick * (long)width;
	sign.right = sign.left + (long)width; 
	sign.top = 0;
	sign.bottom = m_BmpFinishPtr->GetHeight(); 

	GAME_ENGINE->DrawBitmap(m_BmpFinishPtr,6000, 283, sign);
}
