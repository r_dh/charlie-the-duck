#pragma once
//-----------------------------------------------------
// Name, first name: D'Heygere, R�my
// Group: 1DAE7
//-----------------------------------------------------

//-----------------------------------------------------
// Include Files
//-----------------------------------------------------
#include "GameEngine.h"

//-----------------------------------------------------
// Item Class									
//-----------------------------------------------------
class Item
{
public:
	Item(DOUBLE2 spawnPos, int ItemID, bool movement);	// Constructor
	virtual ~Item();		// Destructor

	//-------------------------------------------------
	// Methods - Member functions							
	//-------------------------------------------------
	virtual void Paint() = 0;
	virtual void Tick() = 0;
	DOUBLE2 GetPos();
	bool GetMovement();


protected: 
	//-------------------------------------------------
	// Datamembers								
	//-------------------------------------------------
	DOUBLE2 m_ItemPos;
	int m_ItemID;
	bool m_DoesMove;

	static const int
		BOX_ID = 0,
		COIN_ID = 1,
		DIAMOND_ID = 2,
		HEART_ID = 3;
	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	Item(const Item& yRef);									
	Item& operator=(const Item& yRef);	
};

 
