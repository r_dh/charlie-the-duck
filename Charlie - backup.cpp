//-----------------------------------------------------
// Name, first name: D'Heygere, R�my
// Group: 1DAE7
//-----------------------------------------------------

//---------------------------
// Includes
//---------------------------
#include "Charlie.h"

//---------------------------
// Defines
//---------------------------
#define GAME_ENGINE (GameEngine::GetSingleton())
//initalisaie m_CameraPos aangepast 0,95->0 //Paint(), matCamPos(0,-95)->0,0 aangepast // offset in MoveCharlie aangepast 108->0
//---------------------------
// Constructor & Destructor
//---------------------------
Charlie::Charlie(DOUBLE2 pos, Bitmap *bmpCharlie,  DOUBLE2 levelSize):m_BmpCharliePtr(bmpCharlie), m_PosCharlie(pos),
	m_animationTick(0),
	m_animationCounter(0),
	m_ScaleSize(1.2),
	m_Scale(false),
	m_Airborne(false),
	m_Immobilised(false),
	m_BlockRight(false),
	m_BlockLeft(false),
	m_CharlieSpeed(300),
	m_CameraPos(0,95), // ,95
	m_LevelSize(levelSize),
	m_Gravity(1200),
	m_ActorVelocity(0),
	m_GotHit(false),
	m_HitCounter(-1),
	m_AudJumpPtr(nullptr),
	m_AudHitPtr(nullptr),
	m_Lifes(3),
	m_MovingAlong(false)
{
	m_BmpCharliePtr->SetTransparencyColor(255,0,255);

	m_HitLevelPtr = new HitRegion();
	m_HitLevelPtr->CreateFromSVG("./resources1/Background.svg");//move to constructor, +see charliegame

	m_HitActorVPtr = new HitRegion();
	m_HitActorVPtr->CreateFromRect(5,5,40,56); //(5,5,40,56); //5,0,50,56

	m_HitActorHPtr = new HitRegion();
	m_HitActorHPtr->CreateFromRect(5, 50, 40, 56);//L T R B //5, 50, 40, 56

	//Audio
	m_AudJumpPtr  = new Audio("./resources1/Sounds/Jump.mp3");
	m_AudHitPtr  = new Audio("./resources1/Sounds/Hit.wav");


	//GAME_ENGINE->ConsoleCreate();
	// nothing to create
}

Charlie::~Charlie()
{
	delete m_HitLevelPtr;
	delete m_HitActorVPtr;
	delete m_HitActorHPtr;

	delete m_AudJumpPtr;
	delete m_AudHitPtr;
	//delete m_BmpCharliePtr;
	// nothing to destroy
}

//---------------------------
// Methods - Member functions
//---------------------------

// Add here the methods - Member functions


void Charlie::Paint(){
	GAME_ENGINE->SetColor(COLOR(255,0,0,100)); //Color for hitregion

	//GAME_ENGINE->FillHitRegion(m_HitActorVPtr);
	//GAME_ENGINE->FillHitRegion(m_HitActorHPtr);

	if (m_CharlieState == CharlieState::WAITING)m_animationTick = NR_WAITING_CLIPS;
	if (m_CharlieState == CharlieState::SWIMMING)m_animationTick = NR_CLIP_SWIMMING;

	RECT	clipSize;

	clipSize.left =  m_animationTick * CLIP_WIDTH; //
	clipSize.right = clipSize.left + CLIP_WIDTH;
	clipSize.top = 0;
	clipSize.bottom = CLIP_HEIGHT;


	MATRIX3X2 matCenter, matTranslate, matScale, matPosition, matWorldTransform, matCamPos;

	// formula for translating regular bitmap
	if(!m_Scale)matTranslate.SetAsTranslate(clipSize.left * m_ScaleSize + CLIP_WIDTH * m_ScaleSize /2, m_BmpCharliePtr->GetHeight()* m_ScaleSize /2);
	// formula for translating scaled (mirrored) bitmap
	if(m_Scale)matTranslate.SetAsTranslate(m_BmpCharliePtr->GetWidth() * m_ScaleSize /2-(clipSize.left * m_ScaleSize + CLIP_WIDTH * m_ScaleSize /2) - CLIP_WIDTH *m_ScaleSize * 3, // -CLIP_WIDTH*3 ? don't even mention, magic
		m_BmpCharliePtr->GetHeight()* m_ScaleSize /2);

	matCenter.SetAsTranslate(-(clipSize.left + CLIP_WIDTH /2), -m_BmpCharliePtr->GetHeight() /2);//translatie niet van volledige bmp, enkel van clip
	matScale.SetAsScale(m_Scale ? -m_ScaleSize : m_ScaleSize, m_ScaleSize); //1.6
	matPosition.SetAsTranslate(m_PosCharlie);
	matCamPos.SetAsTranslate(-m_CameraPos.x, -m_CameraPos.y + 95);//+ 95 // , 0 //, m_CameraPos.y // -m_CameraPos.y  //hier geen Item, enkel achtergrond (<-wrong!!!)
	matWorldTransform = matCenter * matScale * matTranslate * matPosition * matCamPos;

	GAME_ENGINE->SetTransformMatrix(matWorldTransform);


	GAME_ENGINE->DrawBitmap(m_BmpCharliePtr, 0, 0, clipSize);

	m_HitActorVPtr->SetPos(0,0);//(0,0) //0,108
	m_HitActorHPtr->SetPos(0,0);//(0,0)
	//GAME_ENGINE->SetTransformMatrix(matCamPos.Inverse());
	//	GAME_ENGINE->FillHitRegion(m_HitActorVPtr);
	//	GAME_ENGINE->FillHitRegion(m_HitActorHPtr);
	//GAME_ENGINE->DrawLine(clipSize.left + CLIP_WIDTH /2,0,clipSize.left + CLIP_WIDTH /2,50); //DEBUG LINE
}

void Charlie::Tick(double deltaTime){

	m_AudJumpPtr->Tick();
	m_AudHitPtr->Tick();

	if(!(m_CharlieState == CharlieState::JUMPING) &&
		!(m_CharlieState == CharlieState::SWIMMING) &&
		!(m_CharlieState == CharlieState::DYING) &&
		!m_MovingAlong) m_CharlieState = CharlieState::WAITING;

	//m_BlockRight, m_BlockLeft
	if(GAME_ENGINE->IsKeyDown(VK_RIGHT) && !m_BlockRight && !m_Immobilised){//!m_Immobilised
		m_BlockLeft = false;
		if(!m_Airborne) m_CharlieState = CharlieState::WALKING;
		//	m_ScaleInt = 1;
		m_Scale = false;
		m_PosCharlie.x += m_CharlieSpeed * deltaTime;
	}

	if(GAME_ENGINE->IsKeyDown(VK_LEFT) && !m_BlockLeft && !m_Immobilised) {//!m_Immobilised
		m_BlockRight = false;
		m_Scale = true;
		if(m_PosCharlie.x > 0){
			if(!m_Airborne) m_CharlieState = CharlieState::WALKING;
			m_PosCharlie.x -= m_CharlieSpeed * deltaTime;
		}
	}

	//	if(GAME_ENGINE->IsKeyDown('D')) m_CharlieState = CharlieState::DYING;
	//	if(GAME_ENGINE->IsKeyDown('S')) m_CharlieState = CharlieState::SWIMMING;




	//m_CharlieState = CharlieState::WALKING;
	switch (m_CharlieState)
	{
	case Charlie::CharlieState::WAITING:
		m_animationTick = NR_WAITING_CLIPS;
		break;
	case Charlie::CharlieState::WALKING:
		++m_animationCounter;
		m_animationTick = m_animationTick % 2;

		break;
	case Charlie::CharlieState::SWIMMING:
		m_animationTick = NR_CLIP_SWIMMING;
		break;
	case Charlie::CharlieState::DYING:
		m_animationTick = NR_CLIP_DYING;
		break;
	case Charlie::CharlieState::JUMPING:
		//uncomment commented code, comment uncommented code for implementing jump
		//++m_animationCounter;//met maxHeight ect.
		//TEST
		m_BlockLeft = false;
		m_BlockRight = false;
		//END TEST

		//DEBUG CODE, TESTING ANIMATION ONLY
		if (!(m_CharlieState == CharlieState::SWIMMING)){
			if (m_animationCounter < 20) {
				++m_animationCounter;//met maxheight, later on

			}
			else {
				++m_animationTick;
				m_animationTick = m_animationTick % 4;
				m_animationCounter = m_animationCounter % 10;
				/*if(m_HitCounter >= 0) ++m_HitCounter;
				if(m_HitCounter >= 5) m_HitCounter =-1;
				if(m_GotHit && m_HitCounter == -1) m_GotHit = false;*/
			}
		}
		//END DEBUG CODE

		//m_animationTick = m_animationTick % 4;//NR_JUMPING_CLIPS; 2& 3: jumping/ landen: frame 1 (0,1,2,3)
		if(m_animationTick == 3)m_CharlieState = CharlieState::WAITING;// veranderen naar ivm landen ect.!
		break;
	}

	if(m_HitCounter >= 0) ++m_HitCounter;
	if(m_HitCounter >= 240) m_HitCounter =-1;
	if(m_GotHit && m_HitCounter == -1){
		m_BmpCharliePtr->SetOpacity(1);
		m_GotHit = false;
	}
	//if (!(m_CharlieState == CharlieState::SWIMMING)){
	if(m_animationCounter == 8 && !(m_CharlieState == CharlieState::JUMPING)){
		++m_animationTick;
		m_animationTick = m_animationTick % NR_CLIPS;
	}
	if(!(m_CharlieState == CharlieState::JUMPING)) m_animationCounter = m_animationCounter % 10;

	//}
	//	GAME_ENGINE->ConsolePrintString(String("Tick: ") + m_animationTick,0,1);
	//	GAME_ENGINE->ConsolePrintString(String("Counter: ") + m_animationCounter,0,2);

	//camera naar rechts als Charlie over de helft is (en beweegt) (2->helft scherm)
	if(GAME_ENGINE->GetWidth()/2 <= m_PosCharlie.x - m_CameraPos.x && GAME_ENGINE->IsKeyDown(VK_RIGHT))m_CameraPos.x += m_CharlieSpeed * deltaTime;

	//camera naar links als Charlie bij de rand is (en beweegt) (4->finetuning "rand")
	if(GAME_ENGINE->GetWidth()/4 >= m_PosCharlie.x - m_CameraPos.x && GAME_ENGINE->IsKeyDown(VK_LEFT))m_CameraPos.x -= m_CharlieSpeed * deltaTime;

	int offset = 108; //108 //height Duck
	//camera naar onder als Charlie over de helft is (2->helft scherm) && niet buiten level
	//m_Airborne == test
	int camOffSet = (int)m_PosCharlie.y + offset - (int)m_CameraPos.y;
	int camOffSetEnd = (int)m_PosCharlie.y + offset + (int)m_CameraPos.y;
	if(GAME_ENGINE->GetHeight()/4*3 <= camOffSet && m_Airborne && camOffSetEnd < m_LevelSize.y && m_ActorVelocity > 0){
		m_CameraPos.y += m_ActorVelocity * deltaTime; //TROUBLE //*poef*, werkt!
	}

	//camera naar boven als Charlie bij de rand is  (4->finetuning "rand")
	//	int nr = GAME_ENGINE->GetHeight()/4;  //  + offset - m_CameraPos.y
	if( GAME_ENGINE->GetHeight()/4 >= m_PosCharlie.y && m_Airborne && m_ActorVelocity < 0) {
		m_CameraPos.y += m_ActorVelocity * deltaTime;
		//m_CameraPos.y -= (GAME_ENGINE->GetHeight()/4 - (m_CameraPos.y - m_PosCharlie.y));//(m_PosCharlie.y + offset - m_CameraPos.y));
	}
	//{

	//	int engineVal =  GAME_ENGINE->GetHeight()/4;
	//	int offSet = m_PosCharlie.y + offset - m_CameraPos.y;
	//	int min = engineVal - offSet;//m_CameraPos.y += m_ActorVelocity * deltaTime; //TROUBLE
	//	m_CameraPos.y -= min;
	//	m_CameraPos.y = -300; //TEST TEST
	//}
	//Limit camera position
	if(m_CameraPos.x < 0)m_CameraPos.x = 0;
	//if(m_CameraPos.y < 0)m_CameraPos.y = 0; //NEVER NEVER NEVER NEVER COPY PASTE!!!!  ~35 min time wasted
	if(GAME_ENGINE->GetWidth() > m_LevelSize.x - m_CameraPos.x )
		m_CameraPos.x = m_LevelSize.x-GAME_ENGINE->GetWidth();

	//	if(m_LevelSize.y - m_CameraPos.y > GAME_ENGINE->GetHeight()) //m_CameraPos.y + 
	//		m_CameraPos.y = GAME_ENGINE->GetHeight() - m_LevelSize.y;
}


//void Charlie::SetCamPos(double camPos){
//	m_CameraPos = camPos;
//}


void Charlie::GetState(){//enum

}

void Charlie::SetState(){

}

void Charlie::KeyPressed(TCHAR cKey){
	if(cKey == VK_SPACE && !m_Airborne ){ //&& !(m_CharlieState == CharlieState::JUMPING) && m_ActorVelocity<=10
		m_Airborne = true;
		m_CharlieState = CharlieState::JUMPING;
		m_ActorVelocity = -600; //-400
		m_animationTick = 2;
		m_BlockLeft = false;
		m_BlockRight = false;
		m_AudJumpPtr->Play();
	}
	//if(cKey == VK_SPACE)m_ActorVelocity = -400;


}


void Charlie::SetCharliePos(DOUBLE2 positie){
	m_PosCharlie = positie;
}

DOUBLE2 Charlie::GetCharliePos(){
	return m_PosCharlie;
}

void Charlie::SetPosCam(DOUBLE2 posCam){
	//OutputDebugString(String("Camera pos: ") + m_CameraPos + "\n");
	m_CameraPos = posCam;
	//OutputDebugString(String("Camera pos set") + m_CameraPos + "\n");
}



DOUBLE2 Charlie::GetPosCam(){
	return m_CameraPos;
}

void Charlie::MoveCharlie(double deltaTime, HitRegion *levelRegion){
	int offset = 108;//108; //108 //0

	m_HitActorHPtr->SetPos(m_PosCharlie.x, m_PosCharlie.y + offset); // + offset
	m_HitActorVPtr->SetPos(m_PosCharlie.x, m_PosCharlie.y + offset); //+ offset


	RECT2 hitArea = m_HitActorVPtr->CollisionTest(levelRegion);
	RECT2 hitAreaH = m_HitActorHPtr->CollisionTest(levelRegion);
	//m_HitActorVPtr->
	RECT2 actorRect = m_HitActorVPtr->GetBounds();
	RECT2 actorRectH = m_HitActorHPtr->GetBounds();
	//m_BlockRight, m_BlockLeft
	m_Immobilised = false;
	if(hitArea.bottom - hitArea.top > 0){ //geraakt
				OutputDebugString("Initi�le hittest\n");

		if (abs((hitArea.bottom - hitArea.top) - (actorRect.bottom - actorRect.top)) < 0.1) { //geraakt langs de zijkant //TEST met H
			//			m_Immobilised = true;
			double hitRegionWidth = hitAreaH.right - hitAreaH.left;
			//			OutputDebugString(String("zijkant; hit width: ") + hitRegionWidth + "\n");

			if(abs(hitAreaH.right - actorRectH.right) < 0.1){// && GAME_ENGINE->IsKeyDown(VK_RIGHT) //geraakt/vast langs rechts? //almossttt
				m_BlockRight = true;
				m_BlockLeft = false;
				m_PosCharlie.x -= abs(hitAreaH.right - actorRectH.right);//m_CharlieSpeed * deltaTime;
				m_CharlieState = CharlieState::WAITING;
				//				OutputDebugString("rechts\n");
			}
			else if (abs(hitAreaH.left - actorRectH.left) < 0.1){ // && GAME_ENGINE->IsKeyDown(VK_LEFT) // geraakt/vast langs links? //work to be done
				m_BlockLeft = true;
				m_BlockRight = false;
				m_PosCharlie.x += abs(hitAreaH.left - actorRectH.left);//m_CharlieSpeed * deltaTime;
				m_CharlieState = CharlieState::WAITING;
				//				OutputDebugString("links\n");
			}
		}
		//geraakt (enkel!) langs de onderkant (&& niet top)
		if (abs(hitArea.bottom - actorRect.bottom) < 0.1 && !(abs((hitArea.bottom - hitArea.top) - (actorRect.bottom - actorRect.top)) < 0.1)){
			m_PosCharlie.y -= (hitArea.bottom - hitArea.top);
			m_Airborne = false;
			if(m_ActorVelocity>=0) m_ActorVelocity = 0;
			OutputDebugString("Hit Bot\n");
		}

		//		else if(abs(hitArea.top - actorRect.top) < 0.1){ //geraakt aan bovenkant (maakt eigelijk niet uit, mag met hoofd langs/door hitregions wandelen)
		//			OutputDebugString("bovenkant\n");
		//			m_ActorVelocity += m_Gravity * deltaTime;
		//			m_PosCharlie.y += m_ActorVelocity * deltaTime;
		//			if(m_ActorVelocity > 50 || m_ActorVelocity < -50) m_Airborne = true;

		//		}
		//		else { //actor niet geraakt aan voeten of --h-o-o-f-d-- maar in het midden (opnieuw.. bestaat al)
		//			OutputDebugString("midden\n");
		//			m_ActorVelocity += m_Gravity * deltaTime; 
		//			m_PosCharlie.y += m_ActorVelocity * deltaTime; 
		//			if(m_ActorVelocity > 50 || m_ActorVelocity < -50) m_Airborne = true; 
		//		}
		//		if(m_ActorVelocity>=0) m_ActorVelocity = 0;
		//		OutputDebugString("Hit1\n");
	}

	//	else if(hitAreaH.right - hitAreaH.left > 0){ //er is een hit bij de voeten
	//		if (abs(hitAreaH.right - actorRectH.right) < 0.1){ //langs rechts (niet enkel rechts?)
	//			m_PosCharlie.x -= (hitAreaH.right - hitAreaH.left);
	//			OutputDebugString("voeten (+rechts?)\n");
	//		}
	//		else if((abs(hitAreaH.left - actorRectH.left)<0.1)){ //langs links
	//			m_PosCharlie.x += (hitAreaH.right - hitAreaH.left);
	//		}
	//		OutputDebugString("voeten (+links?)\n");
	//	}

	else if(m_PosCharlie.y + m_BmpCharliePtr->GetHeight()* m_ScaleSize - m_CameraPos.y>= GAME_ENGINE->GetHeight() && m_ActorVelocity >= 0){//niet uit scherm vallen
		m_PosCharlie.y = GAME_ENGINE->GetHeight()-m_BmpCharliePtr->GetHeight()- 10;
		if(m_ActorVelocity>=0) m_ActorVelocity = 0;
		m_Airborne = false;
		m_CharlieState = CharlieState::SWIMMING;
				OutputDebugString("Fallprotection\n");
	}
	else {//val
		m_ActorVelocity += m_Gravity * deltaTime;
		m_PosCharlie.y += m_ActorVelocity * deltaTime;
		if(m_ActorVelocity > 50 || m_ActorVelocity < -50) m_Airborne = true;
		else m_Airborne = false;
				OutputDebugString("Airborne\n");
	}

	m_HitActorHPtr->SetPos(m_PosCharlie.x, m_PosCharlie.y + offset);
	m_HitActorVPtr->SetPos(m_PosCharlie.x, m_PosCharlie.y + offset);
}

/*
void Charlie::MoveCharlie(double deltaTime, HitRegion *levelRegion){
	//raycast verticaal
        HIT hitArr[1];
        DOUBLE2 vector;
        vector.x = 0;
        vector.y = CLIP_HEIGHT * m_ScaleSize -15;
        
        int amountIntersection = levelRegion->Raycast(m_PosCharlie + DOUBLE2(0, 15), vector, hitArr, 1);
        if (amountIntersection != 0){//als er hits zijn, dan avatar naar boven zetten
            m_PosCharlie.y -= (1-hitArr[0].lambda) * vector.Length();//vermenigvulding met de lengte van vector om in pixels te zetten
            m_ActorVelocity=0;
            m_Airborne = false;
        }
        else{
            m_Airborne = true;
        }
        
        //horizontaal
        DOUBLE2 vectorH;
        double offset=12;
    
        vectorH.x =- (CLIP_WIDTH/2 - offset); //m_BmpBigAvatarPtr->GetWidth()/6 /2
        vectorH.y = 0;
        amountIntersection=levelRegion->Raycast(m_PosCharlie + DOUBLE2(0,20),vectorH,hitArr,1);
        //linkse vector
        if (amountIntersection != 0){
            m_PosCharlie.x += (1-hitArr[0].lambda) * vectorH.Length();
            m_CharlieSpeed = 0;
        }
        
        vectorH.x= CLIP_WIDTH/2 - offset; //m_BmpBigAvatarPtr->GetWidth()/6 /2
        vectorH.y=0;
        amountIntersection=levelRegion->Raycast(m_PosCharlie + DOUBLE2(0,20), vectorH, hitArr, 1);
        //rechtse vector
        if (amountIntersection != 0){
            m_PosCharlie.x -= (1-hitArr[0].lambda) * vectorH.Length();
            m_CharlieSpeed = 0;
        }
}
*/
bool Charlie::SwimTest(double deltaTime, HitRegion *waterHitregion){
	int offset = 108; //108 //0

	m_HitActorHPtr->SetPos(m_PosCharlie.x, m_PosCharlie.y + offset);
	m_HitActorVPtr->SetPos(m_PosCharlie.x, m_PosCharlie.y + offset);


	RECT2 hitArea = m_HitActorVPtr->CollisionTest(waterHitregion);
	//RECT2 hitAreaH = m_HitActorHPtr->CollisionTest(waterHitregion);

	RECT2 actorRect = m_HitActorVPtr->GetBounds();
	//RECT2 actorRectH = m_HitActorHPtr->GetBounds();

	if(hitArea.bottom - hitArea.top > 0){

		//COPY PASTED
		if(m_ActorVelocity < 0) {
			m_PosCharlie.y += m_ActorVelocity * deltaTime;	
		}

		if(m_ActorVelocity >= 0){
			m_ActorVelocity = 0;
			m_Airborne = false;
			m_Immobilised = true;
			m_PosCharlie.y -= hitArea.bottom - hitArea.top;
			//m_CharlieState = CharlieState::WALKING;
		}
		//END COPY PASTE
		m_CharlieState = CharlieState::SWIMMING;
		//		OutputDebugString("Swimming\n");
		return true;
	}
	else return false;
}

bool Charlie::OnPlatform(double deltaTime, HitRegion *platformHitregion){
	int offset = 108; //108 //0

	m_HitActorHPtr->SetPos(m_PosCharlie.x, m_PosCharlie.y + offset);
	m_HitActorVPtr->SetPos(m_PosCharlie.x, m_PosCharlie.y + offset);


	//RECT2 hitArea = m_HitActorVPtr->CollisionTest(platformHitregion);
	RECT2 hitAreaH = m_HitActorHPtr->CollisionTest(platformHitregion);

	//RECT2 actorRect = m_HitActorVPtr->GetBounds();
	RECT2 actorRectH = m_HitActorHPtr->GetBounds();

	if(hitAreaH.bottom - hitAreaH.top > 0){
		if(m_ActorVelocity < 0) {
			m_PosCharlie.y += m_ActorVelocity * deltaTime;	
		}

		if(m_ActorVelocity >= 0){
			m_ActorVelocity = 0;
			m_Airborne = false;
			m_Immobilised = false;
			m_PosCharlie.y -= hitAreaH.bottom - hitAreaH.top;
			m_CharlieState = CharlieState::WALKING;
		}

		//m_CharlieState = CharlieState::WAITING;
		//		OutputDebugString("Platform\n");	
		return true;
	}
	else return false;
}

RECT2 Charlie::GetActorHPtr(){
	return m_HitActorHPtr->GetBounds();
}

RECT2 Charlie::GetActorVPtr(){
	return m_HitActorVPtr->GetBounds();
}


void Charlie::Bounce(){
	//if(!m_Airborne ){ 
	m_Airborne = true;
	m_CharlieState = CharlieState::JUMPING;
	m_ActorVelocity = -600; //-400
	m_animationTick = 2;
	m_AudJumpPtr->Play();
	//	}
	//m_PosCharlie.y += m_ActorVelocity *;
}

void Charlie::BumpHead(){
	m_ActorVelocity *= -1;
}


void Charlie::BlockedRight(){
	m_BlockRight = true;

}

void Charlie::BlockedLeft(){
	m_BlockLeft = true;
}

bool Charlie::IsFalling(){
	if(m_ActorVelocity > 0) return true;
	else return false;
}

bool Charlie::Hit(){
	if(!m_GotHit){
		m_GotHit = true;
		m_BmpCharliePtr->SetOpacity(0.5);
		m_HitCounter = 0;
		--m_Lifes;
		m_AudHitPtr->Play();
		return m_GotHit;
	}

	return !m_GotHit;
}


bool Charlie::Die(double deltaTime){
	int offset = 108;
	if(!(m_CharlieState == CharlieState::DYING)) m_ActorVelocity = -600;
	m_CharlieState = CharlieState::DYING;
	m_Airborne = true;

	if(m_PosCharlie.y - offset > GAME_ENGINE->GetHeight()) return true;
	else {
		m_ActorVelocity += m_Gravity * deltaTime;
		m_PosCharlie.y += m_ActorVelocity * deltaTime;
	}

	m_HitActorHPtr->SetPos(m_PosCharlie.x, m_PosCharlie.y + offset);
	m_HitActorVPtr->SetPos(m_PosCharlie.x, m_PosCharlie.y + offset);
	return false;
}

bool Charlie::MoveAlong(double deltaTime){
	//	int offset = 108;
	m_MovingAlong = true;
	m_CharlieState = CharlieState::WALKING;
	//m_ActorVelocity += m_Gravity * deltaTime;
	m_PosCharlie.y += m_ActorVelocity * deltaTime;
	if(m_PosCharlie.y > 187) m_PosCharlie.y = 187;

	if (m_PosCharlie.x  > m_LevelSize.x) {
		m_MovingAlong = false;
		return true;	
	}
	else {
		m_PosCharlie.x += m_CharlieSpeed * deltaTime;
		return false;
	}


	//	m_HitActorHPtr->SetPos(m_PosCharlie.x, m_PosCharlie.y + offset);
	//	m_HitActorVPtr->SetPos(m_PosCharlie.x, m_PosCharlie.y + offset);

}
