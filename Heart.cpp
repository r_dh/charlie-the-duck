//-----------------------------------------------------
// Name, first name:
// Group: 1DAE.
//-----------------------------------------------------

//---------------------------
// Includes
//---------------------------
#include "Heart.h"

//---------------------------
// Defines
//---------------------------
#define GAME_ENGINE (GameEngine::GetSingleton())

Bitmap* Heart::m_BmpHeart = nullptr;
int Heart:: m_HeartNr = 0;
//---------------------------
// Constructor & Destructor
//---------------------------
Heart::Heart(DOUBLE2 spawnPos, int ItemID, bool movement):Item(spawnPos, ItemID,  movement)
{
	++m_HeartNr;
	if (m_BmpHeart == nullptr && m_HeartNr > 0){
		m_BmpHeart = new Bitmap("./resources1/Heart.bmp");
		m_BmpHeart->SetTransparencyColor(255,0,255);
	}

	m_ClipWidth.top = 0;
	m_ClipWidth.bottom = 28;
}

Heart::~Heart()
{
	if(m_HeartNr > 1) --m_HeartNr;
	else if(m_HeartNr == 1){
		delete m_BmpHeart;
		m_BmpHeart = nullptr;
		--m_HeartNr;
	}
}

//---------------------------
// Methods - Member functions
//---------------------------

void Heart::Paint(){
	m_ClipWidth.left = m_animationTick * CLIP_WIDTH;
	m_ClipWidth.right = m_ClipWidth.left + CLIP_WIDTH;

	GAME_ENGINE->DrawBitmap(m_BmpHeart, (int)m_ItemPos.x , (int)m_ItemPos.y, m_ClipWidth);
}

void Heart::Tick(){
		++m_animationCounter;
	if(m_animationCounter == 2){
		++m_animationTick;
		m_animationTick = m_animationTick % NR_CLIPS;
	}
	m_animationCounter = m_animationCounter % 30;
}


