#pragma once
//-----------------------------------------------------
// Name, first name: D'Heygere, R�my
// Group: 1DAE7
//-----------------------------------------------------

//-----------------------------------------------------
// Include Files
//-----------------------------------------------------
#include "GameEngine.h"
#include "Unit1.h"
//-----------------------------------------------------
// Unit2 Class									
//-----------------------------------------------------
class Unit2: public Unit1
{
public:
	Unit2(DOUBLE2 spawnPos, int enemyID, DOUBLE2 endPos); // Constructor
	virtual ~Unit2();		// Destructor

	//-------------------------------------------------
	// Methods - Member functions							
	//-------------------------------------------------

	void Paint();
private: 
	//-------------------------------------------------
	// Datamembers								
	//-------------------------------------------------
	static Bitmap *m_BmpUnit2;
	static int m_Unit2Nr;

	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	Unit2(const Unit2& yRef);									
	Unit2& operator=(const Unit2& yRef);	
};

 
