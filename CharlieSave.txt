[Charlie]
CharlieSpawnPositionX=4218.81
CharlieSpawnPositionY=454.297
CameraPosX=3895.8
CameraPosY=136.832
Coins=11
Lifes=3
Hearts=3
Diamonds=8
Score=2430

[CoinPos]
Coin=5834,318
Coin=5874,318
Coin=5914,318
Coin=6106,318 (true)
Coin=6146,318 (true)
Coin=6186,318 (true)

[DiamondPos]
Diamond=5277,320 (true)
Diamond=5317,320 (true)
Diamond=5357,320 (true)

[BoxPos]
Box=2955,319 (coin)
Box=4789,478 (heart)
Box=5029,413 (coin)

[Unit1]

[Unit2]

[Frog]
startPosFrog=4819,442.728;endPosFrog=4867

[Bee]
startPosBee=4819,442.728;
