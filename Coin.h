#pragma once
//-----------------------------------------------------
// Name, first name:
// Group: 1DAE.
//-----------------------------------------------------

//-----------------------------------------------------
// Include Files
//-----------------------------------------------------
#include "GameEngine.h"
#include "Item.h"
//-----------------------------------------------------
// Coin Class									
//-----------------------------------------------------
class Coin: public Item
{
public:
	Coin(DOUBLE2 spawnPos, int ItemID, bool movement);				// Constructor
	virtual ~Coin();		// Destructor

	//-------------------------------------------------
	// Methods - Member functions							
	//-------------------------------------------------
	void Paint();
	void Tick();


private: 
	//-------------------------------------------------
	// Datamembers								
	//------------------------------------------------
	static Bitmap *m_BmpCoin;
	static int m_CoinNr;
	static const int NR_CLIPS = 8,
					CLIP_WIDTH = 32;
	int m_animationTick, m_animationCounter;
	RECT m_ClipWidth;
	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	Coin(const Coin& yRef);									
	Coin& operator=(const Coin& yRef);	
};


