//-----------------------------------------------------------------
// Game Engine WinMain Function
// C++ Header - GameWinMain.h - version v2_13 jan 2014 
// Copyright Kevin Hoefman, Bart Uyttenhove, Peter Verswyvelen
// http://www.digitalartsandentertainment.be/
//-----------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------
// Include Files
//-----------------------------------------------------------------
#define WIN32_LEAN_AND_MEAN
#include <windows.h>

//-----------------------------------------------------------------
// Windows Function Declarations
//-----------------------------------------------------------------
int _tmain();
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PSTR szCmdLine, int iCmdShow);