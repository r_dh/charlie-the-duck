//-----------------------------------------------------
// Name, first name: D'Heygere, R�my
// Group: 1DAE7
//-----------------------------------------------------

//---------------------------
// Includes
//---------------------------
#include "Charlie.h"

//---------------------------
// Defines
//---------------------------
#define GAME_ENGINE (GameEngine::GetSingleton())
//---------------------------
// Constructor & Destructor
//---------------------------
Charlie::Charlie(DOUBLE2 pos, Bitmap *bmpCharlie,  DOUBLE2 levelSize):m_BmpCharliePtr(bmpCharlie), m_PosCharlie(pos),
	m_animationTick(0),
	m_animationCounter(0),
	m_ScaleSize(1.2),
	m_Scale(false),
	m_Airborne(false),
	m_Immobilised(false),
	m_BlockRight(false),
	m_BlockLeft(false),
	m_CharlieSpeed(300),
	m_CameraPos(0,0),
	m_LevelSize(levelSize),
	m_Gravity(1200),
	m_ActorVelocity(0),
	m_GotHit(false),
	m_HitCounter(-1),
	m_AudJumpPtr(nullptr),
	m_AudHitPtr(nullptr),
	m_Lifes(3),
	m_MovingAlong(false)
{
	m_BmpCharliePtr->SetTransparencyColor(255,0,255);

	//Hitregions
	m_HitLevelPtr = new HitRegion();
	m_HitLevelPtr->CreateFromSVG("./resources1/Background.svg");

	m_HitActorVPtr = new HitRegion();
	m_HitActorVPtr->CreateFromRect(5,5,40,56);

	m_HitActorHPtr = new HitRegion();
	m_HitActorHPtr->CreateFromRect(5, 50, 40, 56);

	//Audio
	m_AudJumpPtr  = new Audio("./resources1/Sounds/Jump.mp3");
	m_AudHitPtr  = new Audio("./resources1/Sounds/Hit.wav");

	//RayCast
	m_RayCastPosArr[0] = DOUBLE2(13, 7);
	m_RayCastPosArr[1] = DOUBLE2(21, 5);
	m_RayCastPosArr[2] = DOUBLE2(30, 5);
	m_RayCastPosArr[3] = DOUBLE2(9, 18);
	m_RayCastPosArr[4] = DOUBLE2(42, 18);
	m_RayCastPosArr[5] = DOUBLE2(3, 33);
	m_RayCastPosArr[6] = DOUBLE2(40, 33);
	m_RayCastPosArr[7] = DOUBLE2(5, 44);
	m_RayCastPosArr[8] = DOUBLE2(38, 42);
	m_RayCastPosArr[9] = DOUBLE2(7, 55);
	m_RayCastPosArr[10] = DOUBLE2(40, 55);
	m_RayCastPosArr[11] = DOUBLE2(15, 56);
	m_RayCastPosArr[12] = DOUBLE2(30, 56);
}

Charlie::~Charlie()
{
	delete m_HitLevelPtr;
	delete m_HitActorVPtr;
	delete m_HitActorHPtr;

	delete m_AudJumpPtr;
	delete m_AudHitPtr;
}

//---------------------------
// Methods - Member functions
//---------------------------

void Charlie::Paint(){
	if (m_CharlieState == CharlieState::WAITING)m_animationTick = NR_WAITING_CLIPS;
	if (m_CharlieState == CharlieState::SWIMMING)m_animationTick = NR_CLIP_SWIMMING;

	RECT	clipSize;
	clipSize.left =  m_animationTick * CLIP_WIDTH;
	clipSize.right = clipSize.left + CLIP_WIDTH;
	clipSize.top = 0;
	clipSize.bottom = CLIP_HEIGHT;

	MATRIX3X2 matCenter, matTranslate, matScale, matPosition, matWorldTransform, matHeightOffset, matCamPos;

	// formula for translating regular bitmap
	if(!m_Scale)matTranslate.SetAsTranslate(clipSize.left * m_ScaleSize + CLIP_WIDTH * m_ScaleSize /2, m_BmpCharliePtr->GetHeight()* m_ScaleSize /2);
	// formula for translating scaled (mirrored) bitmap
	if(m_Scale)matTranslate.SetAsTranslate(m_BmpCharliePtr->GetWidth() * m_ScaleSize / 2  //Get width divided by 2 (center)
		- (clipSize.left * m_ScaleSize + CLIP_WIDTH * m_ScaleSize /2) - CLIP_WIDTH * m_ScaleSize * 3, //Substract mirrored part
		m_BmpCharliePtr->GetHeight()* m_ScaleSize /2);

	matCenter.SetAsTranslate(-(clipSize.left + CLIP_WIDTH /2), -m_BmpCharliePtr->GetHeight() /2);//translatie niet van volledige bmp, enkel van clip
	matScale.SetAsScale(m_Scale ? -m_ScaleSize : m_ScaleSize, m_ScaleSize);
	matPosition.SetAsTranslate(m_PosCharlie);
	matHeightOffset.SetAsTranslate(0,-(CLIP_HEIGHT * m_ScaleSize - CLIP_HEIGHT));
	matCamPos.SetAsTranslate(-m_CameraPos.x, -m_CameraPos.y);
	matWorldTransform = matCenter * matScale * matTranslate * matHeightOffset * matPosition * matCamPos;

	GAME_ENGINE->SetTransformMatrix(matWorldTransform);
	GAME_ENGINE->DrawBitmap(m_BmpCharliePtr, 0, 0, clipSize);
}

void Charlie::Tick(double deltaTime){
	//Audio
	m_AudJumpPtr->Tick();
	m_AudHitPtr->Tick();

	//Animation
	if(!(m_CharlieState == CharlieState::JUMPING) &&
		!(m_CharlieState == CharlieState::SWIMMING) &&
		!(m_CharlieState == CharlieState::DYING) &&
		!m_MovingAlong) m_CharlieState = CharlieState::WAITING;

	//Movement + animation
	m_CharlieSpeed = 0;
	if(GAME_ENGINE->IsKeyDown(VK_RIGHT) && !m_BlockRight && !m_Immobilised){
		m_BlockLeft = false;
		if(!m_Airborne
			&& !(m_CharlieState == CharlieState::SWIMMING)
			&& !(m_CharlieState == CharlieState::DYING)) m_CharlieState = CharlieState::WALKING;

		m_Scale = false;
		m_CharlieSpeed = R_SPEED;
	}

	if(GAME_ENGINE->IsKeyDown(VK_LEFT) && !m_BlockLeft && !m_Immobilised) {
		if(m_CharlieSpeed == 0){
			m_BlockRight = false;
			m_Scale = true;
			if(m_PosCharlie.x > 0){
				if(!m_Airborne
					&& !(m_CharlieState == CharlieState::SWIMMING)
					&& !(m_CharlieState == CharlieState::DYING)) m_CharlieState = CharlieState::WALKING;
				if(m_CharlieSpeed == 0) m_CharlieSpeed = L_SPEED;
			}
		}
		else {
			m_CharlieSpeed = 0;
			m_CharlieState = CharlieState::WAITING;
		}
	}
	
	//Animation
	switch (m_CharlieState)
	{
	case Charlie::CharlieState::WAITING:
		m_animationTick = NR_WAITING_CLIPS;
		break;
	case Charlie::CharlieState::WALKING:
		++m_animationCounter;
		m_animationTick = m_animationTick % 2;

		break;
	case Charlie::CharlieState::SWIMMING:
		m_animationTick = NR_CLIP_SWIMMING;
		break;
	case Charlie::CharlieState::DYING:
		m_animationTick = NR_CLIP_DYING;
		break;
	case Charlie::CharlieState::JUMPING:
		
		m_BlockLeft = false;
		m_BlockRight = false;

		if (!(m_CharlieState == CharlieState::SWIMMING)){
			if (m_animationCounter < 20) {//20->Finetuning
				++m_animationCounter;
			}
			else {
				++m_animationTick;
				m_animationTick = m_animationTick % 4;//4->Finetuning
				m_animationCounter = m_animationCounter % ANIMATION_CTR;
			}
		}

		if(m_animationTick == 3) m_CharlieState = CharlieState::WAITING;
		break;
	}

	//Got hit (transparant) animation
	if(m_HitCounter >= 0) ++m_HitCounter;
	if(m_HitCounter >= HIT_TICKS) m_HitCounter =-1;
	if(m_GotHit && m_HitCounter == -1){
		m_BmpCharliePtr->SetOpacity(1);
		m_GotHit = false;
	}
	
	//8->finetuning
	if(m_animationCounter == 8 && !(m_CharlieState == CharlieState::JUMPING)){
		++m_animationTick;
		m_animationTick = m_animationTick % NR_CLIPS;
	}
	
	if(!(m_CharlieState == CharlieState::JUMPING)) m_animationCounter = m_animationCounter % ANIMATION_CTR;

	//camera naar rechts als Charlie over de helft van het scherm is (en beweegt) (2->helft scherm)
	if(GAME_ENGINE->GetWidth()/2 <= m_PosCharlie.x - m_CameraPos.x && GAME_ENGINE->IsKeyDown(VK_RIGHT))m_CameraPos.x += m_CharlieSpeed * deltaTime;

	//camera naar links als Charlie bij de rand is (en beweegt) (4->finetuning "rand")
	if(GAME_ENGINE->GetWidth()/4 >= m_PosCharlie.x - m_CameraPos.x && GAME_ENGINE->IsKeyDown(VK_LEFT))m_CameraPos.x += m_CharlieSpeed * deltaTime;

	//camera naar onder als Charlie over de rand is (4*3 of 75% = "rand") && niet buiten level is
	int camOffSet = (int)m_PosCharlie.y - (int)m_CameraPos.y;
	int camOffSetEnd = (int)m_PosCharlie.y + (int)m_CameraPos.y;
	if(GAME_ENGINE->GetHeight()/4*3 <= camOffSet && m_Airborne && camOffSetEnd < m_LevelSize.y && m_ActorVelocity > 0){
		m_CameraPos.y += m_ActorVelocity * deltaTime;
	}

	//camera naar boven als Charlie bij de rand is  (3->finetuning "rand")
	if( GAME_ENGINE->GetHeight()/3 >= m_PosCharlie.y && m_Airborne && m_ActorVelocity < 0) {
		m_CameraPos.y += m_ActorVelocity * deltaTime;
	}

	//Limit camera position
	if(m_CameraPos.x < 0) m_CameraPos.x = 0;
	if(GAME_ENGINE->GetWidth() > m_LevelSize.x - m_CameraPos.x )
		m_CameraPos.x = m_LevelSize.x-GAME_ENGINE->GetWidth();
}

void Charlie::KeyPressed(TCHAR cKey){
	if(cKey == VK_SPACE && m_ActorVelocity < 50 && m_ActorVelocity > -50 ){
		m_Airborne = true;
		m_CharlieState = CharlieState::JUMPING;
		m_ActorVelocity = -600;
		m_animationTick = 2;
		m_BlockLeft = false;
		m_BlockRight = false;
		m_AudJumpPtr->Play();
	}
}

void Charlie::SetCharliePos(DOUBLE2 positie){
	m_CharlieState = CharlieState::WAITING;
	m_PosCharlie = positie;
}

DOUBLE2 Charlie::GetCharliePos(){
	return m_PosCharlie;
}

void Charlie::SetPosCam(DOUBLE2 posCam){
	m_CameraPos = posCam;
}

DOUBLE2 Charlie::GetPosCam(){
	return m_CameraPos;
}

void Charlie::MoveCharlie(double deltaTime, HitRegion *levelRegion){
	DOUBLE2 displacement;
	HIT hitArr[1];

	m_ActorVelocity += m_Gravity * deltaTime;
	DOUBLE2 charlieVelocity = DOUBLE2(0, m_ActorVelocity);//vertical check
	DOUBLE2 vector = charlieVelocity * deltaTime;

	for (int i = 0; i < RAY_POINTS; i++){
		displacement = m_RayCastPosArr[i];

		if(levelRegion->Raycast(m_PosCharlie + displacement, vector, hitArr, 1, 0) > 0){
			m_PosCharlie.y -= (1-hitArr[0].lambda) * vector.Length();
			if (m_ActorVelocity > 0) m_ActorVelocity = 0;
			m_Airborne = false;
		}
		else if(m_ActorVelocity > 50) m_Airborne = true;
	}
	m_PosCharlie.y += m_ActorVelocity * deltaTime;

	charlieVelocity = DOUBLE2(m_CharlieSpeed, 0); //horizontal check
	vector = charlieVelocity * deltaTime;

	for (int i = 0; i < RAY_POINTS; i++){
		displacement = m_RayCastPosArr[i];
		if(levelRegion->Raycast(m_PosCharlie + displacement, vector, hitArr, 1, 0) > 0){
			if(!(m_CharlieState == CharlieState::SWIMMING))m_CharlieState = CharlieState::WAITING;
			if(m_CharlieSpeed > 0) m_PosCharlie.x -= (1-hitArr[0].lambda) * vector.Length();
			else if(m_CharlieSpeed < 0)  m_PosCharlie.x += (1-hitArr[0].lambda) * vector.Length();
			m_CharlieSpeed = 0;

		}
	}
	m_PosCharlie.x += m_CharlieSpeed * deltaTime;

	m_HitActorHPtr->SetPos(m_PosCharlie.x, m_PosCharlie.y);
	m_HitActorVPtr->SetPos(m_PosCharlie.x, m_PosCharlie.y);
}

bool Charlie::SwimTest(double deltaTime, HitRegion *waterHitregion){
	m_HitActorHPtr->SetPos(m_PosCharlie.x, m_PosCharlie.y);
	m_HitActorVPtr->SetPos(m_PosCharlie.x, m_PosCharlie.y);

	RECT2 hitArea = m_HitActorVPtr->CollisionTest(waterHitregion);
	RECT2 actorRect = m_HitActorVPtr->GetBounds();

	if(hitArea.bottom - hitArea.top > 0){
		if(m_ActorVelocity < 0) {
			m_PosCharlie.y += m_ActorVelocity * deltaTime;	
		}
		if(m_ActorVelocity >= 0){
			m_ActorVelocity = 0;
			m_Airborne = false;
			m_PosCharlie.y -= hitArea.bottom - hitArea.top;
		}
		m_CharlieState = CharlieState::SWIMMING;
		return true;
	}
	else return false;
}

bool Charlie::OnPlatform(double deltaTime, HitRegion *platformHitregion){
	m_HitActorHPtr->SetPos(m_PosCharlie.x, m_PosCharlie.y);
	m_HitActorVPtr->SetPos(m_PosCharlie.x, m_PosCharlie.y);

	RECT2 hitAreaH = m_HitActorHPtr->CollisionTest(platformHitregion);
	RECT2 actorRectH = m_HitActorHPtr->GetBounds();

	if(hitAreaH.bottom - hitAreaH.top > 0){
		if(m_ActorVelocity < 0) {
			m_PosCharlie.y += m_ActorVelocity * deltaTime;	
		}
		if(m_ActorVelocity >= 0){
			m_ActorVelocity = 0;
			m_Airborne = false;
			m_Immobilised = false;
			m_PosCharlie.y -= hitAreaH.bottom - hitAreaH.top;
			m_CharlieState = CharlieState::WALKING;
		}
		return true;
	}
	else return false;
}

RECT2 Charlie::GetActorHPtr(){
	return m_HitActorHPtr->GetBounds();
}

RECT2 Charlie::GetActorVPtr(){
	return m_HitActorVPtr->GetBounds();
}

void Charlie::Bounce(){
	m_Airborne = true;
	m_CharlieState = CharlieState::JUMPING;
	m_ActorVelocity = -600;
	m_animationTick = 2;
	m_AudJumpPtr->Play();
}

void Charlie::BumpHead(){
	m_ActorVelocity *= -1;
}


void Charlie::BlockedRight(){
	m_BlockRight = true;

}

void Charlie::BlockedLeft(){
	m_BlockLeft = true;
}

bool Charlie::IsFalling(){
	if(m_ActorVelocity > 0) return true;
	else return false;
}

bool Charlie::Hit(){
	if(!m_GotHit){
		m_GotHit = true;
		m_BmpCharliePtr->SetOpacity(0.5);
		m_HitCounter = 0;
		--m_Lifes;
		m_AudHitPtr->Play();
		return m_GotHit;
	}
	return !m_GotHit;
}

bool Charlie::Die(double deltaTime){

	if(!(m_CharlieState == CharlieState::DYING)) m_ActorVelocity = -600;
	m_CharlieState = CharlieState::DYING;
	m_Airborne = true;

	if(m_PosCharlie.y > GAME_ENGINE->GetHeight()*2) return true; //dood
	else {
		m_ActorVelocity += m_Gravity * deltaTime;
		m_PosCharlie.y += m_ActorVelocity * deltaTime;
	}

	m_HitActorHPtr->SetPos(m_PosCharlie.x, m_PosCharlie.y);
	m_HitActorVPtr->SetPos(m_PosCharlie.x, m_PosCharlie.y);
	return false;
}

bool Charlie::MoveAlong(double deltaTime){
	m_MovingAlong = true;
	m_CharlieState = CharlieState::WALKING;


	if (m_PosCharlie.x  > m_LevelSize.x) {
		m_MovingAlong = false;
		return true;	
	}
	else {
		m_CharlieSpeed = R_SPEED;
		m_PosCharlie.x += m_CharlieSpeed * deltaTime;

		m_ActorVelocity += m_Gravity * deltaTime;
		m_PosCharlie.y += m_ActorVelocity * deltaTime;

		if(m_PosCharlie.y > LVL_HEIGHT) m_PosCharlie.y = LVL_HEIGHT;

		m_HitActorHPtr->SetPos(m_PosCharlie.x, m_PosCharlie.y);
		m_HitActorVPtr->SetPos(m_PosCharlie.x, m_PosCharlie.y);
		return false;
	}
}
