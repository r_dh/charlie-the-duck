#pragma once
//-----------------------------------------------------
// Name, first name: D'Heygere, R�my
// Group: 1DAE7
//-----------------------------------------------------

//-----------------------------------------------------
// Include Files
//-----------------------------------------------------
#include "GameEngine.h"
#include <string>
#include <iostream>
#include "tchar.h"
#include <fstream>

//-----------------------------------------------------
// Charlie Class									
//-----------------------------------------------------
class Charlie
{
public:
	Charlie(DOUBLE2 pos, Bitmap *bmpCharlie, DOUBLE2 levelSize); // Constructor
	virtual ~Charlie();		// Destructor

	//-------------------------------------------------
	// Methods - Member functions							
	//------------------------------------------------
	void KeyPressed(TCHAR cKey);
	void Paint();
	void Tick(double deltaTime);

	void SetCharliePos(DOUBLE2 positie);
	DOUBLE2 GetCharliePos();

	void SetPosCam(DOUBLE2 posCam);
	DOUBLE2 GetPosCam();

	void MoveCharlie(double deltaTime, HitRegion *levelRegion);
	bool SwimTest(double deltaTime, HitRegion *waterHitregion);
	bool OnPlatform(double deltaTime, HitRegion *platformHitregion);
	bool IsFalling();

	void Bounce();
	void BumpHead();

	void BlockedRight();
	void BlockedLeft();

	bool Hit();
	bool Die(double deltaTime);

	bool MoveAlong(double deltaTime);

	RECT2 GetActorHPtr();
	RECT2 GetActorVPtr();

private:
	
	enum class CharlieState: int
	{
		WAITING,					
		WALKING,					
		SWIMMING,		
		DYING,	
		JUMPING,			
	}; 

	//-------------------------------------------------
	// Datamembers								
	//-------------------------------------------------
	Bitmap *m_BmpCharliePtr;
	HitRegion *m_HitActorHPtr, *m_HitActorVPtr, *m_HitLevelPtr;
	Audio *m_AudJumpPtr, *m_AudHitPtr;
	CharlieState m_CharlieState;
	DOUBLE2 m_PosCharlie, m_CameraPos, m_LevelSize;

	int m_animationTick, m_animationCounter, m_HitCounter, m_Lifes;
	double m_ScaleSize, m_CharlieSpeed, m_Gravity, m_ActorVelocity;
	bool  
		m_Scale, m_Airborne, m_Immobilised, m_BlockRight,
		m_BlockLeft, m_GotHit, m_MovingAlong;

	static const int RAY_POINTS = 13;
	DOUBLE2 m_RayCastPosArr[RAY_POINTS];

	static const int 
		CLIP_WIDTH = 46,
		CLIP_HEIGHT = 57,
		NR_CLIP_DYING = 4,
		NR_CLIP_SWIMMING = 6,
		NR_CLIPS = 8;
	static const int
		NR_WAITING_CLIPS = 0,
		NR_WALKING_CLIPS = 4,
		NR_SWIMMING_CLIPS = 1,
		NR_JUMPING_CLIPS = 2,
		NR_DYING_CLIPS = 1;
	static const int
		R_SPEED = 300,
		L_SPEED = -300,
		LVL_HEIGHT = 293,
		HIT_TICKS = 240,
		ANIMATION_CTR = 10;

	//-------------------------------------------------
	// Methods								
	//-------------------------------------------------

	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	Charlie(const Charlie& yRef);									
	Charlie& operator=(const Charlie& yRef);	
};

 
