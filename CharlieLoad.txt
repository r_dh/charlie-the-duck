[Charlie]
CharlieSpawnPositionX=150
CharlieSpawnPositionY=280.953
CameraPosX=0
CameraPosY=32.2913


[CoinPos]
Coin=240,255
Coin=280,255
Coin=320,255

Coin=1205,448 (true)

Coin=1844,287 (true)
Coin=1884,287 (true)
Coin=1924,287 (true)

Coin=4234,478

Coin=5834,318
Coin=5874,318
Coin=5914,318

Coin=6106,318 (true)
Coin=6146,318 (true)
Coin=6186,318 (true)


[DiamondPos]
Diamond=500,355 (true)
Diamond=525,355 (true)
Diamond=550,355 (true)

Diamond=3000,450 (true)

Diamond=5277,320 (true)
Diamond=5317,320 (true)
Diamond=5357,320 (true)


[BoxPos]
Box=838,478 (coin)
Box=878,478 (coin)

Box=1918,478 (diamond)
Box=1958,478 (heart)

Box=2316,478 (coin)
Box=2316,448 (diamond)

Box=2955,319 (coin)

Box=3913,478 (diamond)
Box=3952,478 (diamond)

Box=4789,478 (heart)

Box=5029,413 (coin)

[Unit1] (2)
startPosU1=595,480;endPosU1=1200
startPosU1=995,320;endPosU1=1153

[Unit2] (2)
startPosU2=1400,480;endPosU2=1680
startPosU2=3637,382;endPosU2=3907

[Frog] (4)
startPosFrog=3035,500;endPosFrog=3516
startPosFrog=3912,470;endPosFrog=4909


[Bee] (11)
startPosBee=745,385;
startPosBee=1337,196;
startPosBee=2126,384;
startPosBee=2442,294;
startPosBee=3318,330;
startPosBee=3285,100;
startPosBee=3754,60;
startPosBee=3996,308;
startPosBee=4811,360;
startPosBee=4795,197;
startPosBee=5336,239;