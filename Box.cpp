//-----------------------------------------------------
// Name, first name:
// Group: 1DAE.
//-----------------------------------------------------

//---------------------------
// Includes
//---------------------------
#include "Box.h"

//---------------------------
// Defines
//---------------------------
#define GAME_ENGINE (GameEngine::GetSingleton())
Bitmap* Box::m_BmpBox = nullptr;
int Box:: m_BoxNr = 0;
//---------------------------
// Constructor & Destructor
//---------------------------
Box::Box(DOUBLE2 spawnPos, int ItemID, int ItemIDInside):Item(spawnPos, ItemID,  false), m_ItemInside(ItemIDInside)
{ 
	if (m_BmpBox == nullptr){
		m_BmpBox = new Bitmap("./resources1/Box.bmp");
		m_BmpBox->SetTransparencyColor(255,0,255);
	}
	++m_BoxNr;

	m_ClipWidth.top = 0;
	m_ClipWidth.bottom = 33;
}

Box::~Box()
{
	if(m_BoxNr > 1) --m_BoxNr;
	else if(m_BoxNr == 1){
		delete m_BmpBox;
		m_BmpBox = nullptr;
		--m_BoxNr;
	}
}

//---------------------------
// Methods - Member functions
//---------------------------

// Add here the methods - Member functions

void Box::Paint(){
	m_ClipWidth.left = m_animationTick * CLIP_WIDTH;
	m_ClipWidth.right = m_ClipWidth.left + CLIP_WIDTH;

	GAME_ENGINE->DrawBitmap(m_BmpBox, (int)m_ItemPos.x , (int)m_ItemPos.y, m_ClipWidth);
}

void Box::Tick(){

	++m_animationCounter;
	if(m_animationCounter == 2){
		++m_animationTick;
		m_animationTick = m_animationTick % NR_CLIPS;
	}
	m_animationCounter = m_animationCounter % 30;
}

int Box::GetItemInside(){
	return m_ItemInside;
}
