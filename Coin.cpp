//-----------------------------------------------------
// Name, first name:
// Group: 1DAE.
//-----------------------------------------------------

//---------------------------
// Includes
//---------------------------
#include "Coin.h"

//---------------------------
// Defines
//---------------------------
#define GAME_ENGINE (GameEngine::GetSingleton())
Bitmap* Coin::m_BmpCoin = nullptr;
int Coin:: m_CoinNr = 0;

//---------------------------
// Constructor & Destructor
//---------------------------
Coin::Coin(DOUBLE2 spawnPos, int ItemID, bool movement):Item(spawnPos, ItemID,  movement)
{
	if (m_BmpCoin == nullptr && m_CoinNr > 0){
		m_BmpCoin = new Bitmap("./resources1/Coin1.png");
		m_BmpCoin->SetTransparencyColor(255,0,255);
	}
	++m_CoinNr;
	m_ClipWidth.top = 0;
	m_ClipWidth.bottom = 32;
}

Coin::~Coin()
{
	if(m_CoinNr > 1) --m_CoinNr;
	else if(m_CoinNr == 1){
		delete m_BmpCoin;
		m_BmpCoin = nullptr;
		--m_CoinNr;
	}
}

//---------------------------
// Add here the methods - Member functions
//-------------------------------------------------
void Coin::Paint(){
	m_ClipWidth.left = m_animationTick * CLIP_WIDTH;
	m_ClipWidth.right = m_ClipWidth.left + CLIP_WIDTH;

	GAME_ENGINE->DrawBitmap(m_BmpCoin, (int)m_ItemPos.x , (int)m_ItemPos.y, m_ClipWidth);
}

void Coin::Tick(){
	if(m_animationCounter == 0){
		++m_animationTick;
		m_animationTick = m_animationTick % NR_CLIPS;
	}
	++m_animationCounter;
	m_animationCounter = m_animationCounter % 10;
}

