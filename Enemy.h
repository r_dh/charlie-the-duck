#pragma once
//-----------------------------------------------------
// Name, first name: D'Heygere, R�my
// Group: 1DAE7
//-----------------------------------------------------

//-----------------------------------------------------
// Include Files
//-----------------------------------------------------
#include "GameEngine.h"

//-----------------------------------------------------
// Enemy Class									
//-----------------------------------------------------
class Enemy
{
public:
	Enemy(DOUBLE2 spawnPos, int enemyID, DOUBLE2 endPos); // Constructor
	Enemy(DOUBLE2 spawnPos, int enemyID, RECT flyZone);
	virtual ~Enemy();		// Destructor

	//-------------------------------------------------
	// Methods - Member functions							
	//-------------------------------------------------
	virtual void Paint() = 0;
	virtual void Tick(double deltaTime) = 0;
	DOUBLE2 GetPos();
	void Turn();
	void Kill();

protected:
	//-------------------------------------------------
	// Methods - protected functions							
	//-------------------------------------------------

	//-------------------------------------------------
	// Datamembers								
	//-------------------------------------------------
	DOUBLE2 m_EnemyPos, m_EndPos;
	RECT m_FlyZone;
	int m_EnemyID, m_EndPosX, m_MovementSpeed;
	double m_Gravity, m_EnemyVelocity;
	bool m_SeverelyInjured;

	static Bitmap *m_BmpBee;
	static int  m_FrogNr, m_BeeNr;
	static const int 
		BROWN_ID = 0, //unit1
		PURPLE_ID = 1, //unit2
		FROG_ID = 2,
		BEE_ID = 3;

	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	Enemy(const Enemy& yRef);									
	Enemy& operator=(const Enemy& yRef);	
};


