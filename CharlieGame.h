//-----------------------------------------------------------------
// Game File
// C++ Source - CharlieGame.h - version v2_13 jan 2014 
// Copyright Kevin Hoefman - kevin.hoefman@howest.be
// http://www.digitalartsandentertainment.be/
//-----------------------------------------------------------------

//-----------------------------------------------------------------
// Name, first name: D'Heygere, R�my
// Group: 1DAE7
//-----------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------
// Include Files
//-----------------------------------------------------------------

#include "Resource.h"	
#include "GameEngine.h"
#include "AbstractGame.h"

#include <sstream>
#include <string>
//#include <iostream>
#include "Charlie.h"
#include "Item.h"

#include "Coin.h"
#include "Box.h"
#include "Heart.h"
#include "Diamond.h"

#include "Unit1.h"
#include "Unit2.h"
#include "Frog.h"



//-----------------------------------------------------------------
// CharlieGame Class																
//-----------------------------------------------------------------
class CharlieGame : public AbstractGame, public Callable
{
public:				
	//---------------------------
	// Constructor(s)
	//---------------------------
	CharlieGame();

	//---------------------------
	// Destructor
	//---------------------------
	virtual ~CharlieGame();

	//---------------------------
	// General Methods
	//---------------------------

	void GameInitialize(HINSTANCE hInstance);
	void GameStart();				
	void GameEnd();
	void KeyPressed(TCHAR cKey);
	void GameTick(double deltaTime);
	void GamePaint(RECT rect);

	// -------------------------
	// Member functions
	// -------------------------

private:
	// -------------------------
	// Private functions
	// -------------------------
	void WriteFile(std::string fileName, char sep);
	void ReadFile(std::string fileName, char sep);
	void ProcessLine(std::string line, char sep, int shapeIndex);

	void ProcessItem(std::string line, char sep);//Find Items
	void ProcessEnemy(std::string line, char sep);//Find Enemies

	void CreateItem(int ItemID, int PosX, int PosY, bool movement);
	void CreateBox(int PosX, int PosY, int ItemIDInside);

	void CreateEnemy(int startPosX, int startPosY, int endPosX, int enemyID);
	void CreateBee(int startPosX, int startPosY);

	void DrawWater(int posX, int posY, int length);
	void DrawHitRegions();

	void PositionHitRegions(HitRegion *hitRegion[], int arrLength, int ID, bool isItem);
	void ProcessHitRegions(); //Find hitregions to reposition

	bool CollisionCharlie(RECT2 hitRect2); //return true if collision between given RECT and hero
	void ProcessCollisionsCharlie(); //Process Items and Enemies
	void ProcessItem(const int arrLength, const int ID, HitRegion *hitRegion[]); 
	void ProcessEnemy(const int arrLength, const int ID, HitRegion *hitRegion[]);

	void ItemPickedUp(HitRegion *hitRegion[], int itemID, int arrPos);
	void KillEnemy(HitRegion *hitRegion[], int enemyID, int arrPos);

	void KillOffscreen();
	void DeleteAll();

	void PaintHUD();
	void PaintNumber(int number, int posX, int posY);
	void PaintIcon(int iconPos, int posX, int posY);

	void Die();
	void EndGame();

	void DrawFinish();

	template<typename T>
	void DeletePtr(const int arrLength, T *objectPtr[]){
		for (int i = 0; i < arrLength; i++){
				delete objectPtr[i];
				objectPtr[i] = nullptr;
		}
	}

	template<typename T>
	void CheckOffscreen(const int arrLength, T *enemyPtr[]){
		for (int i = 0; i < arrLength; i++){
			if(enemyPtr[i] != nullptr && enemyPtr[i]->GetPos().y > m_BmpLevel1Ptr->GetHeight()){
				delete enemyPtr[i];
				enemyPtr[i] = nullptr;
			}

		}
	}


	//
	// -------------------------
	// Datamembers
	// -------------------------
	Bitmap *m_BmpCharlieScore, *m_BmpScoreNr, *m_BmpIcons, *m_BmpGratsPtr, *m_BmpGameOverPtr; 
	Bitmap *m_BmpCharliePtr, *m_BmpLevel1Ptr, *m_BmpBGPtr, *m_BmpWaterPtr, *m_BmpFinishPtr;
	Charlie *m_CharliePtr;
	DOUBLE2 m_PosCharlie;
	DOUBLE2 m_CameraPos;
	HitRegion *m_HitLevelPtr, *m_HitWaterPtr, *m_HitPlatformPtr;

	enum class CharlieHit: int
	{
		NONE,					
		BOTTOM,					
		TOP,		
		LEFT,	
		RIGHT,			
	}; 

	CharlieHit m_CharlieHit;

	static const int
		ITEMS = 19,
		ITEMS_BOX = 11,
		ITEMS_COIN = 19, 
		ITEMS_DIAMOND = 10,
		ITEMS_HEART = 2;

	static const int
		BOX_ID = 0,
		COIN_ID = 1,
		DIAMOND_ID = 2,
		HEART_ID = 3;

	Box *m_BoxPtr[ITEMS_BOX];
	Coin *m_CoinPtr[ITEMS_COIN];
	Diamond *m_DiamondPtr[ITEMS_DIAMOND];
	Heart *m_HeartPtr[ITEMS_HEART];

	HitRegion
		*m_HitBoxPtr[ITEMS_BOX],
		*m_HitCoinPtr[ITEMS_COIN],
		*m_HitDiamondPtr[ITEMS_DIAMOND],
		*m_HitHeartPtr[ITEMS_HEART];

	static const int
		UNITS_UNIT1 = 2,
		UNITS_UNIT2 = 2,
		UNITS_FROG = 4,
		UNITS_BEE = 11; 

	static const int
		UNIT1_ID = 0,
		UNIT2_ID = 1,
		FROG_ID = 2;
	//BEE_ID = 3;

	Unit1 *m_Unit1Ptr[UNITS_UNIT1];
	Unit2 *m_Unit2Ptr[UNITS_UNIT2];
	Frog *m_FrogPtr[UNITS_FROG];
	//Bee *m_BeePtr[UNITS_BEE];

	HitRegion
		*m_HitUnit1Ptr[UNITS_UNIT1],
		*m_HitUnit2Ptr[UNITS_UNIT2],
		*m_HitFrogPtr[UNITS_FROG];

	DOUBLE2 m_ItemsPosArr[ITEMS];
	int m_ItemsIDArr[ITEMS];
	bool m_ItemsBoolArr[ITEMS];
	bool m_ItemsTick;

	Audio 
		*m_AudCratePtr,
		*m_AudEnemyPtr,
		*m_AudItemPtr;

	int m_CoinCount, m_Lifes, m_DiamondCount, m_HearthCount, m_Score;
	bool m_IsDead, m_TheEnd, m_DrawEnd, m_GameOver;
	static const int
		SCORE_HEIGHT = 16,
		SCORE_CLIP_WIDTH = 10,
		SCORE_WIDTH = 110,
		ICON_HEIGHT = 24,
		ICON_CLIP_WIDTH = 24,
		ICON_WIDTH = 144;

	int m_animationCounter, m_animationTick;
	static const int WATER_CLIP = 1;

	int m_signCounter, m_signTick;
	static const int SIGN_CLIP = 9;
	// -------------------------
	// Disabling default copy constructor and default assignment operator.
	// If you get a linker error from one of these functions, your class is internally trying to use them. This is
	// an error in your class, these declarations are deliberately made without implementation because they should never be used.
	// -------------------------
	CharlieGame(const CharlieGame& tRef);
	CharlieGame& operator=(const CharlieGame& tRef);
};
