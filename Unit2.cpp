//-----------------------------------------------------
// Name, first name: D'Heygere, R�my
// Group: 1DAE7
//-----------------------------------------------------

//---------------------------
// Includes
//---------------------------
#include "Unit2.h"

//---------------------------
// Defines
//---------------------------
#define GAME_ENGINE (GameEngine::GetSingleton())
Bitmap* Unit2::m_BmpUnit2 = nullptr;
int Unit2:: m_Unit2Nr = 0;
//---------------------------
// Constructor & Destructor
//---------------------------
Unit2::Unit2(DOUBLE2 spawnPos, int enemyID, DOUBLE2 endPos):Unit1(spawnPos, enemyID, endPos)
{
	if (m_BmpUnit2 == nullptr){
		m_BmpUnit2 = new Bitmap("./resources1/Unit2.png");
		
		m_BmpUnit2->SetTransparencyColor(255,0,255);
	}
	++m_Unit2Nr;
}

Unit2::~Unit2()
{
	if(m_Unit2Nr > 1) --m_Unit2Nr;
	else if(m_Unit2Nr == 1){
		delete m_BmpUnit2;
		m_BmpUnit2 = nullptr;
		--m_Unit2Nr;
	}
}

//---------------------------
// Methods - Member functions
//---------------------------

// Add here the methods - Member functions

void Unit2::Paint(){
	GAME_ENGINE->DrawBitmap(m_BmpUnit2, m_EnemyPos);
}

