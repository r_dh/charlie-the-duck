#pragma once
//-----------------------------------------------------
// Name, first name:
// Group: 1DAE.
//-----------------------------------------------------

//-----------------------------------------------------
// Include Files
//-----------------------------------------------------
#include "GameEngine.h"
#include "Item.h"

//-----------------------------------------------------
// Box Class									
//-----------------------------------------------------
class Box : public Item
{
public:
	Box(DOUBLE2 spawnPos, int ItemID, int ItemIDInside);// Constructor
	virtual ~Box();		// Destructor

	//-------------------------------------------------
	// Methods - Member functions							
	//-------------------------------------------------
	void Paint();
	void Tick();
	int GetItemInside();

private: 
	//-------------------------------------------------
	// Datamembers								
	//-------------------------------------------------
	static Bitmap *m_BmpBox;
	static int m_BoxNr;
	static const int
		NR_CLIPS = 2,
		CLIP_WIDTH = 42;

	int m_ItemInside, m_animationTick, m_animationCounter;
	RECT m_ClipWidth;
	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	Box(const Box& yRef);									
	Box& operator=(const Box& yRef);	
};


