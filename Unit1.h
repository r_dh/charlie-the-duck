#pragma once
//-----------------------------------------------------
// Name, first name: D'Heygere, R�my
// Group: 1DAE7
//-----------------------------------------------------

//-----------------------------------------------------
// Include Files
//-----------------------------------------------------
#include "GameEngine.h"
#include "Enemy.h"
//-----------------------------------------------------
// Unit1 Class									
//-----------------------------------------------------
class Unit1: public Enemy
{
public:
	Unit1(DOUBLE2 spawnPos, int enemyID, DOUBLE2 endPos);	// Constructor
	virtual ~Unit1();		// Destructor

	//-------------------------------------------------
	// Methods - Member functions							
	//-------------------------------------------------
	void Paint();// = 0;
	void Tick(double deltaTime);// = 0;
	DOUBLE2 GetPos();
	int GetEndPos();
protected: 
	//-------------------------------------------------
	// Methods - Private functions							
	//-------------------------------------------------



	//-------------------------------------------------
	// Datamembers								
	//-------------------------------------------------
	static Bitmap *m_BmpUnit1;
	static int m_Unit1Nr;

	DOUBLE2 m_EnemyPos;
	int m_StartPosX, m_EndPosX;
	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	Unit1(const Unit1& yRef);									
	Unit1& operator=(const Unit1& yRef);	
};

 
