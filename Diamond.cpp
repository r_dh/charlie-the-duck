//-----------------------------------------------------
// Name, first name:
// Group: 1DAE.
//-----------------------------------------------------

//---------------------------
// Includes
//---------------------------
#include "Diamond.h"

//---------------------------
// Defines
//---------------------------
#define GAME_ENGINE (GameEngine::GetSingleton())
Bitmap* Diamond::m_BmpDiamond = nullptr;
int Diamond:: m_DiamondNr = 0;
//---------------------------
// Constructor & Destructor
//---------------------------
Diamond::Diamond(DOUBLE2 spawnPos, int ItemID, bool movement):Item(spawnPos, ItemID,  movement)
{
	if (m_BmpDiamond == nullptr && m_DiamondNr > 0){
		m_BmpDiamond = new Bitmap("./resources1/Diamond.png");
		m_BmpDiamond->SetTransparencyColor(255,0,255);
	}
	++m_DiamondNr;

	m_ClipWidth.top = 0;
	m_ClipWidth.bottom = 30;
}

Diamond::~Diamond()
{
		if(m_DiamondNr > 1) --m_DiamondNr;
	else if(m_DiamondNr == 1){
		delete m_BmpDiamond;
		m_BmpDiamond = nullptr;
		--m_DiamondNr;
	}
}

//---------------------------
// Methods - Member functions
//---------------------------

void Diamond::Paint(){
	m_ClipWidth.left = m_animationTick * CLIP_WIDTH;
	m_ClipWidth.right = m_ClipWidth.left + CLIP_WIDTH;

	GAME_ENGINE->DrawBitmap(m_BmpDiamond, (int)m_ItemPos.x , (int)m_ItemPos.y, m_ClipWidth);
}

void Diamond::Tick(){
	++m_animationCounter;
	if(m_animationCounter == 2){
		++m_animationTick;
		m_animationTick = m_animationTick % NR_CLIPS;
	}
	m_animationCounter = m_animationCounter % 30;
}
