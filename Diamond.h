#pragma once
//-----------------------------------------------------
// Name, first name:
// Group: 1DAE.
//-----------------------------------------------------

//-----------------------------------------------------
// Include Files
//-----------------------------------------------------
#include "GameEngine.h"
#include "Item.h"
//-----------------------------------------------------
// Diamond Class									
//-----------------------------------------------------
class Diamond: public Item
{
public:
	Diamond(DOUBLE2 spawnPos, int ItemID, bool movement);// Constructor
	virtual ~Diamond();		// Destructor

	//-------------------------------------------------
	// Methods - Member functions							
	//-------------------------------------------------
	void Paint();
	void Tick();


private: 
	//-------------------------------------------------
	// Datamembers								
	//------------------------------------------------
	static Bitmap *m_BmpDiamond;
	static int m_DiamondNr;
	static const int
		NR_CLIPS = 2,
		CLIP_WIDTH = 30;
	int m_animationTick, m_animationCounter;
	RECT m_ClipWidth;

	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	Diamond(const Diamond& yRef);									
	Diamond& operator=(const Diamond& yRef);	
};


