#pragma once
//-----------------------------------------------------
// Name, first name:
// Group: 1DAE.
//-----------------------------------------------------

//-----------------------------------------------------
// Include Files
//-----------------------------------------------------
#include "GameEngine.h"
#include "Item.h"
//-----------------------------------------------------
// Heart Class									
//-----------------------------------------------------
class Heart: public Item
{
public:
	Heart(DOUBLE2 spawnPos, int ItemID, bool movement);	// Constructor
	virtual ~Heart();		// Destructor

	//-------------------------------------------------
	// Methods - Member functions							
	//------------------------------------------------
	void Paint();
	void Tick();

private: 
	//-------------------------------------------------
	// Datamembers								
	//------------------------------------------------
	static Bitmap *m_BmpHeart;
	static int m_HeartNr;
	static const int
		NR_CLIPS = 2,
		CLIP_WIDTH = 30;

	int m_animationTick, m_animationCounter;
	RECT m_ClipWidth;

	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	Heart(const Heart& yRef);									
	Heart& operator=(const Heart& yRef);	
};


