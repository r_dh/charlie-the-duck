//-----------------------------------------------------
// Name, first name: D'Heygere, R�my
// Group: 1DAE7
//-----------------------------------------------------

//---------------------------
// Includes
//---------------------------
#include "Frog.h"

//---------------------------
// Defines
//---------------------------
#define GAME_ENGINE (GameEngine::GetSingleton())

Bitmap* Frog::m_BmpFrog  = nullptr;
int Frog:: m_FrogNr  = 0;
//---------------------------
// Constructor & Destructor
//---------------------------
Frog::Frog(DOUBLE2 spawnPos, int enemyID, DOUBLE2 endPos):Enemy(spawnPos, enemyID, endPos)
	,m_EnemyPos(spawnPos)
	, m_StartPosX((int)spawnPos.x)
	,m_EndPosX((int)endPos.x)
	,m_JumpHeight(0)
	,m_MaxHeight(30)
	,m_MinHeight(0)
	,m_JumpsUp(true)
	
{
	m_MovementSpeed = 95;
	if (m_BmpFrog == nullptr){
		m_BmpFrog = new Bitmap("./resources1/Frog.png");

		m_BmpFrog->SetTransparencyColor(255,0,255);
	}

	if (m_BmpFrog != nullptr) m_EndPosX -= m_BmpFrog->GetWidth()/FROG_CLIPS;

	m_FrogClip.top = 0;
	m_FrogClip.bottom = m_BmpFrog->GetHeight();


	++m_FrogNr;
}

Frog::~Frog()
{
	if(m_FrogNr > 1) --m_FrogNr;
	else if(m_FrogNr == 1){
		delete m_BmpFrog;
		m_BmpFrog = nullptr;
		--m_FrogNr;
	}
}

//---------------------------
// Methods - Member functions
//---------------------------

// Add here the methods - Member functions


void Frog::Paint(){

	m_FrogClip.left = m_animationTick * CLIP_WIDTH;
	m_FrogClip.right = m_FrogClip.left + CLIP_WIDTH;
//	MATRIX3X2 matCenter, matScale, matTranslate, matTransform, matRegular;
//	matCenter.SetAsTranslate(-(m_FrogClip.left + CLIP_WIDTH/2), -m_FrogClip.top/2);
//	matScale.SetAsScale(100 , 100); //m_MovementSpeed > 0 ? 1 : -1
//	matTranslate.SetAsTranslate((int)m_EnemyPos.x, (int)m_EnemyPos.y);
//	matTransform = matCenter * matScale * matTranslate * m_matView;
//	GAME_ENGINE->SetTransformMatrix(matTransform);
	GAME_ENGINE->DrawBitmap(m_BmpFrog, (int)m_EnemyPos.x, (int)m_EnemyPos.y, m_FrogClip); //(int)m_EnemyPos.x, (int)m_EnemyPos.y
//	GAME_ENGINE->SetTransformMatrix(matRegular);

}



void Frog::Tick(double deltaTime){
	//if(m_animationCounter == 2){
	//	++m_animationTick;
	//	m_animationTick = m_animationTick % FROG_CLIPS;
	//}

	//++m_animationCounter;
	//m_animationCounter = m_animationCounter % 30;

	if(m_SeverelyInjured){
		m_EnemyVelocity += m_Gravity * deltaTime;
		m_EnemyPos.y += m_EnemyVelocity * deltaTime;
	}
	else{
		double displacement =  m_MovementSpeed * deltaTime;
		m_EnemyPos.x += displacement;
		displacement = abs(displacement);
		if(m_JumpHeight < m_MaxHeight && m_JumpsUp){ // 
			m_animationTick = 1;
			m_JumpHeight += displacement;
			m_EnemyPos.y -= displacement;
			if(m_JumpHeight >= m_MaxHeight) m_JumpsUp = false;
		}
		else if(m_JumpHeight > m_MinHeight){
			m_animationTick = 0;
			m_JumpHeight -= displacement;
			m_EnemyPos.y += displacement;
			if(m_JumpHeight <= m_MinHeight) m_JumpsUp = true;
		}

		if((int)m_EnemyPos.x <= m_StartPosX && m_MovementSpeed < 0 || (int)m_EnemyPos.x >= m_EndPosX && m_MovementSpeed > 0) Turn();
	}
}

DOUBLE2 Frog::GetPos(){
	return m_EnemyPos;
}

int Frog::GetEndPos(){
	return m_EndPosX;
}

//void Frog::SetMatView(MATRIX3X2 matView){
//	m_matView = matView;
//}
