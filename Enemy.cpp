//-----------------------------------------------------
// Name, first name: D'Heygere, R�my
// Group: 1DAE7
//-----------------------------------------------------

//---------------------------
// Includes
//---------------------------
#include "Enemy.h"

//---------------------------
// Defines
//---------------------------
#define GAME_ENGINE (GameEngine::GetSingleton())

Bitmap* Enemy::m_BmpBee   = nullptr;
int Enemy:: m_BeeNr   = 0;
//---------------------------
// Constructor & Destructor
//---------------------------
Enemy::Enemy(DOUBLE2 spawnPos, int enemyID, DOUBLE2 endPos):m_EnemyPos(spawnPos)
	,m_EnemyID(enemyID)
	,m_EndPos(endPos)
	,m_EndPosX((int)endPos.x)
	,m_MovementSpeed(50)
	,m_Gravity(1200)
	,m_EnemyVelocity(0)
	,m_SeverelyInjured(false)
{
	//Empty
}

Enemy::Enemy(DOUBLE2 spawnPos, int enemyID, RECT flyZone):m_EnemyPos(spawnPos)
	,m_EnemyID(enemyID)
	,m_FlyZone(flyZone)
	,m_MovementSpeed(50)
{
	//Empty
}

Enemy::~Enemy()
{
	if(m_BeeNr > 1) --m_BeeNr;
	else if(m_BeeNr == 1){
		delete m_BmpBee;
		m_BmpBee = nullptr;
		--m_BeeNr;
	}

}

//---------------------------
// Methods - Member functions
//---------------------------

// Add here the methods - Member functions

void Enemy::Paint(){

}

void Enemy::Tick(double deltaTime){

}

DOUBLE2 Enemy::GetPos(){
	return m_EnemyPos;
}

void Enemy::Turn(){
	m_MovementSpeed *= -1;
}

void Enemy::Kill(){
	m_EnemyVelocity = -400;
	m_SeverelyInjured = true;
}
