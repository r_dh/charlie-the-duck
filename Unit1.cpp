//-----------------------------------------------------
// Name, first name: D'Heygere, R�my
// Group: 1DAE7
//-----------------------------------------------------

//---------------------------
// Includes
//---------------------------
#include "Unit1.h"

//---------------------------
// Defines
//---------------------------
#define GAME_ENGINE (GameEngine::GetSingleton())
Bitmap* Unit1::m_BmpUnit1 = nullptr;
int Unit1:: m_Unit1Nr = 0;
//---------------------------
// Constructor & Destructor
//---------------------------
Unit1::Unit1(DOUBLE2 spawnPos, int enemyID, DOUBLE2 endPos):Enemy(spawnPos, enemyID, endPos)
	,m_EnemyPos(spawnPos)
	,m_StartPosX((int)spawnPos.x)
	,m_EndPosX((int)endPos.x)
{
	if (m_BmpUnit1 == nullptr){
		m_BmpUnit1 = new Bitmap("./resources1/Unit1.png");

		m_BmpUnit1->SetTransparencyColor(255, 0, 255);
	}

	if (m_BmpUnit1 != nullptr) m_EndPosX -= m_BmpUnit1->GetWidth();
	++m_Unit1Nr;
}

Unit1::~Unit1()
{
	if(m_Unit1Nr > 1) --m_Unit1Nr;
	else if(m_Unit1Nr == 1){
		delete m_BmpUnit1;
		m_BmpUnit1 = nullptr;
		--m_Unit1Nr;
	}
}

//---------------------------
// Methods - Member functions
//---------------------------

// Add here the methods - Member functions
void Unit1::Paint(){
	GAME_ENGINE->DrawBitmap(m_BmpUnit1, m_EnemyPos);
}

void Unit1::Tick(double deltaTime){
	if(m_SeverelyInjured){
		m_EnemyVelocity += m_Gravity * deltaTime;
		m_EnemyPos.y += m_EnemyVelocity * deltaTime;
	}
	else{
		m_EnemyPos.x += m_MovementSpeed * deltaTime;
		if(m_EnemyPos.x <= m_StartPosX && m_MovementSpeed < 0 || m_EnemyPos.x >= m_EndPosX && m_MovementSpeed > 0) Turn();
	}
}

DOUBLE2 Unit1::GetPos(){
	return m_EnemyPos;
}

int Unit1::GetEndPos(){
	return m_EndPosX;
}
