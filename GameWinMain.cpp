//-----------------------------------------------------------------
// Game Engine WinMain Function
// C++ Source - GameWinMain.cpp - version v2_13 jan 2014 
// Copyright Kevin Hoefman, Bart Uyttenhove, Peter Verswyvelen
// http://www.digitalartsandentertainment.be/
//-----------------------------------------------------------------

//-----------------------------------------------------------------
// Include Files
//-----------------------------------------------------------------
#include "GameWinMain.h"
#include "GameEngine.h"

#include "CharlieGame.h"	

	
//-----------------------------------------------------------------
// Defines
//-----------------------------------------------------------------

#define GAME_ENGINE (GameEngine::GetSingleton())

//-----------------------------------------------------------------
// Windows Functions
//-----------------------------------------------------------------

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PSTR szCmdLine, int iCmdShow)
{

	// Enable run-time memory leak check for debug builds.
	#if defined(DEBUG) | defined(_DEBUG)
		_CrtSetDbgFlag( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );

		//_CrtSetBreakAlloc(111);
	#endif

	/////////////////////////////////////////////////////
	if (GAME_ENGINE == nullptr) return FALSE; // create the game engine and exit if it fails

	int returnValue = 0;

	GAME_ENGINE->SetGame(new CharlieGame());	
	returnValue =  GAME_ENGINE->Run(hInstance, iCmdShow); // run the game engine and return the result

	/////////////////////////////////////////////////////


	return returnValue;

}
