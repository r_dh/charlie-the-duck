//-----------------------------------------------------
// Name, first name: D'Heygere, R�my
// Group: 1DAE7
//-----------------------------------------------------

//---------------------------
// Includes
//---------------------------
#include "Item.h"

//---------------------------
// Defines
//---------------------------
#define GAME_ENGINE (GameEngine::GetSingleton())
//Bitmap* Item::m_BmpItem = nullptr;
//Bitmap* Item::m_BmpBox = nullptr;
//Bitmap* Item::m_BmpCoin = nullptr;
//Bitmap* Item::m_BmpDiamond = nullptr;
//Bitmap* Item::m_BmpHeart = nullptr;

//int Item:: m_BoxNr = 0;
//int Item:: m_CoinNr = 0;
//int Item:: m_DiamondNr = 0;
//int Item:: m_HeartNr = 0;
//---------------------------
// Constructor & Destructor
//---------------------------


Item::Item(DOUBLE2 spawnPos, int ItemID, bool movement):m_ItemPos(spawnPos), m_ItemID(ItemID), m_DoesMove(movement)
{
	//Empty
}

Item::~Item()
{
	//Empty
}

//---------------------------
// Methods - Member functions
//---------------------------


void Item::Paint(){

}

void Item::Tick(){
	
}

DOUBLE2 Item::GetPos(){
	return m_ItemPos;
}

bool Item::GetMovement(){
	return m_DoesMove;
}
