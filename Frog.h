#pragma once
//-----------------------------------------------------
// Name, first name:
// Group: 1DAE.
//-----------------------------------------------------

//-----------------------------------------------------
// Include Files
//-----------------------------------------------------
#include "GameEngine.h"
#include "Enemy.h"
//-----------------------------------------------------
// Frog Class									
//-----------------------------------------------------
class Frog: public Enemy
{
public:
	Frog(DOUBLE2 spawnPos, int enemyID, DOUBLE2 endPos);				// Constructor
	virtual ~Frog();		// Destructor

	//-------------------------------------------------
	// Methods - Member functions							
	//-------------------------------------------------
	void Paint();
	void Tick(double deltaTime);
	DOUBLE2 GetPos();
	int GetEndPos();
	//void SetMatView(MATRIX3X2 matView);
private:
	//-------------------------------------------------
	// Methods - Private functions							
	//-------------------------------------------------

	//-------------------------------------------------
	// Datamembers								
	//-------------------------------------------------
	static Bitmap *m_BmpFrog;
	static int  m_FrogNr;
	static const int
		FROG_CLIPS = 2, 
		CLIP_WIDTH = 42;

	bool m_JumpsUp;
	double m_JumpHeight;
	int m_animationCounter, m_animationTick, m_MaxHeight, m_MinHeight;
	RECT m_FrogClip;
	//MATRIX3X2 m_matView;
	DOUBLE2 m_EnemyPos;

	int m_StartPosX, m_EndPosX;
	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	Frog(const Frog& yRef);									
	Frog& operator=(const Frog& yRef);	
};


